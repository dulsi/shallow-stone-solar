#ifndef __SDLSCALE_H
#define __SDLSCALE_H

void SDLScale_RenderCopy(SDL_Renderer *renderer, SDL_Texture *texture, const SDL_Rect *srcrect, const SDL_Rect *dstrect);
void SDLScale_RenderDrawLine(SDL_Renderer *renderer, int x1, int y1, int x2, int y2);
void SDLScale_RenderDrawRect(SDL_Renderer *renderer, SDL_Rect *dstrect);
void SDLScale_Set(double w, double h);

#endif
