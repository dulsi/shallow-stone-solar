#include "uimenu.h"
#include "client.h"
#include "sdlscale.h"

UIMenuItem::UIMenuItem(int xPos, int yPos, const std::string &text)
: x(xPos), y(yPos), textStatic(text), textStaticTex(NULL), textDynamicTex(NULL), textStaticW(0), textStaticH(0), textDynamicW(0), textDynamicH(0)
{
}

void UIMenuItem::createTexture(SDL_Renderer *r, TTF_Font *font)
{
	if (textStaticTex == NULL)
	{
		SDL_Color c;
		c.a = 255;
		c.r = 255;
		c.g = 255;
		c.b = 255;
		SDL_Surface *img = TTF_RenderUTF8_Solid(font, textStatic.c_str(), c);
		textStaticTex = SDL_CreateTextureFromSurface(r, img);
		textStaticW = img->w;
		textStaticH = img->h;
		SDL_FreeSurface(img);
	}
}

void UIMenuItem::destroyTexture()
{
	SDL_DestroyTexture(textStaticTex);
	textStaticTex = NULL;
	if (textDynamicTex != NULL)
	{
		SDL_DestroyTexture(textDynamicTex);
		textDynamicTex = NULL;
		textDynamic = "";
	}
}

void UIMenuItem::setDynamic(SDL_Renderer *r, TTF_Font *font, const std::string &text)
{
	textDynamic = text;
	if (textStaticTex != NULL)
	{
		SDL_DestroyTexture(textDynamicTex);
		textDynamicTex = NULL;
	}
	if (textDynamic != "")
	{
		SDL_Color c;
		c.a = 255;
		c.r = 255;
		c.g = 255;
		c.b = 255;
		SDL_Surface *img = TTF_RenderUTF8_Solid(font, textDynamic.c_str(), c);
		textDynamicTex = SDL_CreateTextureFromSurface(r, img);
		textDynamicW = img->w;
		textDynamicH = img->h;
		SDL_FreeSurface(img);
	}
}

UIMenu::UIMenu(SDL_Renderer *r, TTF_Font *font, std::vector<UIMenuItem> *i)
: menuState(0), items(i), f(font), moveCooldown(0)
{
	for (auto &item : *items)
	{
		item.createTexture(r, font);
	}
}

UIMenu::~UIMenu()
{
	for (auto &item : *items)
	{
		item.destroyTexture();
	}
}

int UIMenu::process(ClientController &control)
{
	if (moveCooldown > 0)
		moveCooldown--;
	if (control.xButton > 0)
	{
		return 1;
	}
	if (moveCooldown == 0)
	{
		if (control.yDir == 2)
		{
			if (menuState > 0)
			{
				menuState--;
				moveCooldown = 2;
			}
		}
		else if (control.yDir == 1)
		{
			if (menuState < items->size() - 1)
			{
				menuState++;
				moveCooldown = 2;
			}
		}
	}
	return 0;
}

void UIMenu::draw(SDL_Renderer *r)
{
	SDL_Rect dst;
	for (auto &item : *items)
	{
		dst.w = item.textStaticW;
		dst.h = item.textStaticH;
		dst.y = item.y;
		dst.x = item.x;
		SDLScale_RenderCopy(r, item.textStaticTex, NULL, &dst);
		if (item.textDynamicTex)
		{
			dst.w = item.textDynamicW;
			dst.h = item.textDynamicH;
			dst.y = item.y;
			dst.x = item.x + 170;
			SDLScale_RenderCopy(r, item.textDynamicTex, NULL, &dst);
		}
	}
	SDL_SetRenderDrawColor(r, 255, 255, 255, 255);
	dst.x = (*items)[menuState].x - 10;
	dst.y = (*items)[menuState].y - 5;
	dst.w = 400;
	dst.h = 30;
	SDLScale_RenderDrawRect(r, &dst);
	SDL_SetRenderDrawColor(r, 0, 0, 0, 255);
}
