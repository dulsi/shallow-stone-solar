#include "uikeyselect.h"
#include "client.h"
#include "sdlscale.h"

const char *prompt[15] = {
	"Left",
	"Right",
	"Up",
	"Down",
	"Look Left",
	"Look Right",
	"Look Up",
	"Look Down",
	"Left Trigger",
	"Right Trigger",
	"A",
	"X",
	"B",
	"Y",
	"Option"
};

UIKeySelect::UIKeySelect(SDL_Renderer *r, TTF_Font *font, ClientController *cc)
: f(font), control(cc), current(0)
{
	SDL_Color c;
	c.a = 255;
	c.r = 255;
	c.g = 255;
	c.b = 255;

	for (int i = 0; i < 15; i++)
	{
		SDL_Surface *img = TTF_RenderUTF8_Solid(f, prompt[i], c);
		promptTex[i] = SDL_CreateTextureFromSurface(r, img);
		promptSize[i][0] = img->w;
		promptSize[i][1] = img->h;
		SDL_FreeSurface(img);
		keyTex[i] = NULL;
	}
}

UIKeySelect::~UIKeySelect()
{
	for (int i = 0; i < 15; i++)
	{
		SDL_DestroyTexture(promptTex[i]);
		if (keyTex[i] != NULL)
			SDL_DestroyTexture(keyTex[i]);
	}
}

void UIKeySelect::process(SDL_Event &sdlevent)
{
	if (sdlevent.type == SDL_KEYDOWN)
	{
		for (int i = 0; i < current; i++)
		{
			if (keys[i] == sdlevent.key.keysym.sym)
				return;
		}
		keys[current] = sdlevent.key.keysym.sym;
		current++;
	}
}

int UIKeySelect::process(ClientController &control1)
{
	if (current == 15)
	{
		control->keyLeft = keys[0];
		control->keyRight = keys[1];
		control->keyUp = keys[2];
		control->keyDown = keys[3];
		control->keyLookLeft = keys[4];
		control->keyLookRight = keys[5];
		control->keyLookUp = keys[6];
		control->keyLookDown = keys[7];
		control->keyL2 = keys[8];
		control->keyR2 = keys[9];
		control->keyX = keys[10];
		control->keySquare = keys[11];
		control->keyCircle = keys[12];
		control->keyTriangle = keys[13];
		control->keyOption = keys[14];
		control->keyDefault = false;
		return 1;
	}
	return 0;
}

void UIKeySelect::draw(SDL_Renderer *r)
{
	SDL_RenderClear(r);
	for (int i = 0; i <= current; i++)
	{
		SDL_Rect dst;
		dst.w = promptSize[i][0];
		dst.h = promptSize[i][1];
		dst.y = 30 + i * 30;
		dst.x = 30;
		SDLScale_RenderCopy(r, promptTex[i], NULL, &dst);

		if (keyTex[i] == NULL)
		{
			const char *name = SDL_GetKeyName(keys[i]);
			if (name[0] != 0)
			{
				SDL_Color c;
				c.a = 255;
				c.r = 255;
				c.g = 255;
				c.b = 255;
				SDL_Surface *img = TTF_RenderUTF8_Solid(f, name, c);
				keyTex[i] = SDL_CreateTextureFromSurface(r, img);
				keySize[i][0] = img->w;
				keySize[i][1] = img->h;
				SDL_FreeSurface(img);
			}
		}
		if (keyTex[i] != NULL)
		{
			dst.w = keySize[i][0];
			dst.h = keySize[i][1];
			dst.x = 200;
			SDLScale_RenderCopy(r, keyTex[i], NULL, &dst);
		}
	}
}
