#include "minerules.h"
#include "message.h"

MineRule::MineRule(int ct, int nt, int fc)
: currentTile(ct), nextTile(nt), fuelCrystals(fc)
{
}

MineRules::MineRules()
{
	rules.push_back(MineRule(Constants::BlockFuelCrystal, 0, 1));
	rules.push_back(MineRule(Constants::BlockUnbreakable, -1, 0));
}

MineRule *MineRules::getRule(int tile)
{
	for (auto &r : rules)
	{
		if (r.currentTile == tile)
			return &r;
	}
	return NULL;
}
