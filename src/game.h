#ifndef __GAME_H
#define __GAME_H

#include <unordered_map>
#include <memory>
#include "cavegenerator.h"
#include "controller.h"
#include "gamelistener.h"
#include "player.h"
#include "minerules.h"
#include "enemy.h"
#include "pathfind/pathfind.h"

class Game : public MapWeight
{
public:
	Game();
	void addListener(GameListener *l);
	void runTurn();
	std::shared_ptr<Controller> getController(const std::string &name);
	bool isPassable(int x, int y);
	std::byte getWeight(int x, int y);
	bool mine(int x, int y, Player *p);
	int nextId();

private:
	std::pair<int, int> findStartPosition();
	void mapChange(int chunkX, int chunkY, CaveChunk &chunk, int x, int y, int block);

public:
	std::unordered_map<int, CaveChunk> world;
	std::vector<Player> players;
	std::vector<Enemy> enemies;
	GameListener *listener;
	int nId;

private:
	CaveGenerator c;
	MineRules rules;
	int spawnChance;

	static const int startChunkX;
	static const int startChunkY;
};

#endif
