#include <random>
#include "randomutil.h"

static std::random_device *dev = NULL;
static std::mt19937 *rng = NULL;

RandomUtil::RandomUtil(int low, int high)
{
	if (dev == NULL)
	{
		dev = new std::random_device();
		rng = new std::mt19937((*dev)());
	}
	std::uniform_int_distribution<std::mt19937::result_type> *dist = new std::uniform_int_distribution<std::mt19937::result_type>(low, high);
	impl = reinterpret_cast<void *>(dist);
}

RandomUtil::~RandomUtil()
{
	std::uniform_int_distribution<std::mt19937::result_type> *dist = reinterpret_cast<std::uniform_int_distribution<std::mt19937::result_type>*>(impl);
	delete dist;
}

int RandomUtil::roll()
{
	std::uniform_int_distribution<std::mt19937::result_type> *dist = reinterpret_cast<std::uniform_int_distribution<std::mt19937::result_type>*>(impl);
	return (*dist)(*rng);
}
