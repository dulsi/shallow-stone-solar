#include <SDL.h>
#include <SDL_net.h>
#include "enemystate.h"
#include "message.h"

int EnemyState::messageSize(12);

EnemyState::EnemyState(int ident, int t, int xPos, int yPos)
: id(ident), type(t), x(xPos), y(yPos), sprite(0)
{
	switch (type)
	{
		case Constants::EnemyAnt:
			health = 20;
			sprite = 1;
			break;
		case Constants::EnemyGlowStick:
			health = 200;
			sprite = 2;
			break;
		default:
			health = 0;
			break;
	}
}

EnemyState::EnemyState(int ident, char *message)
: id(ident)
{
	readMessage(message);
}

void EnemyState::readMessage(char *message)
{
	// id already read and set
	type = SDLNet_Read16(message + 2);
	x = SDLNet_Read16(message + 4);
	y = SDLNet_Read16(message + 6);
	health = SDLNet_Read16(message + 8);
	sprite = SDLNet_Read16(message + 10);
}

void EnemyState::writeMessage(char *message)
{
	SDLNet_Write16(id, message);
	SDLNet_Write16(type, message + 2);
	SDLNet_Write16(x, message + 4);
	SDLNet_Write16(y, message + 6);
	SDLNet_Write16(health, message + 8);
	SDLNet_Write16(sprite, message + 10);
}
