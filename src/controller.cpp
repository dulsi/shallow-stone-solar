#include "controller.h"
#include "message.h"

Controller::Controller()
: yDir(0), xDir(0), lookXDir(0), lookYDir(0), action(0), targetId(-1)
{
}

bool Controller::processMessages(int len, char *message)
{
	// Add buffer handling
	int start = 0;
	while (len > 0)
	{
		int cmd = SDLNet_Read16(message + start);
		switch (cmd)
		{
			case Message::MovePlayer:
				xDir = SDLNet_Read16(message + start + 2);
				yDir = SDLNet_Read16(message + start + 4);
				start += 6;
				len -= 6;
				break;
			case Message::LookPlayer:
				lookXDir = SDLNet_Read16(message + start + 2);
				lookYDir = SDLNet_Read16(message + start + 4);
				start += 6;
				len -= 6;
				break;
			case Message::ActionPlayer:
				action = SDLNet_Read16(message + start + 2);
				targetId = SDLNet_Read16(message + start + 4);
				start += 6;
				len -= 6;
				break;
			case Message::Shutdown:
				return true;
				break;
			default:
				break;
		}
	}
	return false;
}
