#include "uitextinput.h"
#include "client.h"
#include "sdlscale.h"

char keys[2][4][10] = {
	{
		{ '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' },
		{ 'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p' },
		{ 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', '\'' },
		{ 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '?' }
	},
	{
		{ '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' },
		{ 'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P' },
		{ 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', '"' },
		{ 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '-', '_', '/' }
	},
};

SDL_Surface *keyImg[2] = { NULL, NULL };

UITextInput::UITextInput(SDL_Renderer *r, TTF_Font *font, int x1, int y1, const std::string &initial)
: x(x1), y(y1), shift(0), pX(0), pY(0), text(initial), f(font), moveCooldown(0), actionCooldown(0), prevtext(initial)
{
	SDL_Color c[2];
	c[0].a = 255;
	c[0].r = 255;
	c[0].g = 255;
	c[0].b = 255;
	c[1].a = 255;
	c[1].r = 64;
	c[1].g = 64;
	c[1].b = 64;
	char n[2];
	n[1] = 0;
	Uint32 rmask, gmask, bmask, amask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	rmask = 0xff000000;
	gmask = 0x00ff0000;
	bmask = 0x0000ff00;
	amask = 0x000000ff;
#else
	rmask = 0x000000ff;
	gmask = 0x0000ff00;
	bmask = 0x00ff0000;
	amask = 0xff000000;
#endif

	SDL_Rect dst;
	for (int i = 0; i < 2; i++)
	{
		if (keyImg[i] == NULL)
		{
			keyImg[i] = SDL_CreateRGBSurface(0, 400, 400, 32, rmask, gmask, bmask, amask);
			for (int j = 0; j < 4; j++)
			{
				for (int k = 0; k < 10; k++)
				{
					dst.x = k * 40 + 1;
					dst.y = j * 40 + 1;
					dst.w = 38;
					dst.h = 38;
					SDL_FillRect(keyImg[i], &dst, SDL_MapRGB(keyImg[i]->format, c[1].r, c[1].g, c[1].b));
					n[0] = keys[i][j][k];
					SDL_Surface *img = TTF_RenderUTF8_Solid(f, n, c[0]);
					dst.x += (38 - img->w) / 2;
					dst.y += (38 - img->h) / 2;
					dst.w = img->w;
					dst.h = img->h;
					SDL_BlitSurface(img, NULL, keyImg[i], &dst);
					SDL_FreeSurface(img);
				}
			}
		}
		keyTex[i] = SDL_CreateTextureFromSurface(r, keyImg[i]);
	}
	currentTex = NULL;
	if (!text.empty())
	{
		SDL_Surface *img = TTF_RenderUTF8_Solid(f, text.c_str(), c[0]);
		currentTex = SDL_CreateTextureFromSurface(r, img);
		cW = img->w;
		cH = img->h;
		SDL_FreeSurface(img);
	}
}

UITextInput::~UITextInput()
{
	SDL_DestroyTexture(keyTex[0]);
	SDL_DestroyTexture(keyTex[1]);
	SDL_DestroyTexture(currentTex);
}

int UITextInput::process(ClientController &control)
{
	if (moveCooldown > 0)
		moveCooldown--;
	if (moveCooldown == 0)
	{
		if (control.xDir == 1 && pX < 9)
		{
			pX++;
			moveCooldown = 2;
		}
		else if (control.xDir == 2 && pX > 0)
		{
			pX--;
			moveCooldown = 2;
		}
		if (control.yDir == 1 && pY < 3)
		{
			pY++;
			moveCooldown = 2;
		}
		else if (control.yDir == 2 && pY > 0)
		{
			pY--;
			moveCooldown = 2;
		}
	}
	if (actionCooldown > 0)
		actionCooldown--;
	if (actionCooldown == 0)
	{
		if (control.r2Button > 0)
			return 1;
		if (control.xButton > 0)
		{
			text += keys[shift][pY][pX];
			actionCooldown = 2;
		}
		if (control.squareButton > 0)
		{
			if (text.length() > 0)
				text.pop_back();
			actionCooldown = 2;
		}
		if (control.l2Button > 0)
		{
			shift = (shift + 1) % 2;
			actionCooldown = 2;
		}
		if (control.triangleButton > 0)
		{
			text += ' ';
			actionCooldown = 2;
		}
		if (control.circleButton > 0)
		{
			return -1;
		}
	}
	return 0;
}

void UITextInput::draw(SDL_Renderer *r)
{
	SDL_Rect dst;
	dst.w = 404;
	dst.h = 300;
	dst.y = y;
	dst.x = x;
	SDL_RenderFillRect(r, &dst);
	SDL_SetRenderDrawColor(r, 64, 64, 64, 255);
	SDLScale_RenderDrawRect(r, &dst);
	SDL_SetRenderDrawColor(r, 0, 0, 0, 255);
	dst.w = 400;
	dst.h = 400;
	dst.y = y + 40;
	dst.x = x + 2;
	SDLScale_RenderCopy(r, keyTex[shift], NULL, &dst);
	if (pY < 4)
	{
		SDL_SetRenderDrawColor(r, 255, 255, 255, 255);
		dst.x = x + pX * 40 + 2;
		dst.y = y + pY * 40 + 40;
		dst.w = 40;
		dst.h = 40;
		SDLScale_RenderDrawRect(r, &dst);
		SDL_SetRenderDrawColor(r, 0, 0, 0, 255);
	}
	if (prevtext != text)
	{
		if (currentTex)
			SDL_DestroyTexture(currentTex);
		currentTex = NULL;
		if (!text.empty())
		{
			SDL_Color c;
			c.a = 255;
			c.r = 255;
			c.g = 255;
			c.b = 255;
			SDL_Surface *img = TTF_RenderUTF8_Solid(f, text.c_str(), c);
			currentTex = SDL_CreateTextureFromSurface(r, img);
			cW = img->w;
			cH = img->h;
			SDL_FreeSurface(img);
		}
		prevtext = text;
	}
	if (currentTex)
	{
		dst.w = cW;
		dst.h = cH;
		dst.y = y;
		dst.x = x + 40;
		SDLScale_RenderCopy(r, currentTex, NULL, &dst);
	}
}
