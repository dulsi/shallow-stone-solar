#include "randomutil.h"
#include "cavegenerator.h"
#include "message.h"
#include <cstdio>

int CaveChunk::w(40);
int CaveChunk::h(22);

CaveChunk::CaveChunk()
{
	caveMap.resize(w * h, std::byte(0));
}

int CaveChunk::getAdjacentWalls(int x,int y,int scopeX,int scopeY)
{
	int startX = x - scopeX;
	int startY = y - scopeY;
	int endX = x + scopeX;
	int endY = y + scopeY;

	int iX = startX;
	int iY = startY;

	int wallCounter = 0;

	for(iY = startY; iY <= endY; iY++)
	{
		for(iX = startX; iX <= endX; iX++)
		{
			if(!(iX==x && iY==y))
			{
				if(isWall(iX,iY))
				{
					wallCounter += 1;
				}
			}
		}
	}
	return wallCounter;
}

bool CaveChunk::isWall(int x,int y)
{
	// Consider out-of-bound a wall
	if (isOutOfBounds(x,y))
	{
		return true;
	}

	if (caveMap[x + y * w] != std::byte(0))
	{
		return true;
	}

	if (caveMap[x + y * w] == std::byte(0))
	{
		return false;
	}
	return false;
}

bool CaveChunk::isOutOfBounds(int x, int y)
{
	if (x<0 || y<0)
	{
		return true;
	}
	else if (x>w-1 || y>h-1)
	{
		return true;
	}
	return false;
}

void CaveChunk::followWall(int &x, int &y, int &dir)
{
	int loop = 0;
	for (int testDir = dir; loop < 4; testDir = (testDir + 2) % 8)
	{
		if (!isWall(x + Constants::facingMap[testDir][0], y + Constants::facingMap[testDir][1]))
		{
			x += Constants::facingMap[testDir][0];
			y += Constants::facingMap[testDir][1];
			dir = (testDir + 6) % 8;
			break;
		}
		loop++;
	}
}

CaveGenerator::CaveGenerator()
{
	percentAreWalls = 40;
}

void CaveGenerator::addMinerals(CaveChunk &caveMap)
{
	RandomUtil dist(0, 7);
	for (int num = dist.roll() / 2 + 1; num > 0; num--)
	{
		int dir = dist.roll();
		int x = caveMap.w / 2 + Constants::facingMap[dir][0];
		int y = caveMap.h / 2 + Constants::facingMap[dir][1];
		while (caveMap.isWall(x, y))
		{
			x += Constants::facingMap[dir][0];
			y += Constants::facingMap[dir][1];
			if (caveMap.isOutOfBounds(x, y))
				break;
		}
		if (caveMap.isOutOfBounds(x, y))
			continue;
		while (!caveMap.isWall(x + Constants::facingMap[dir][0], y + Constants::facingMap[dir][1]))
		{
			x += Constants::facingMap[dir][0];
			y += Constants::facingMap[dir][1];
			if (caveMap.isOutOfBounds(x, y))
				break;
		}
		if (dir % 2 == 1)
			dir = (dir + 1) % 8;
		while (!caveMap.isWall(x + Constants::facingMap[dir][0], y + Constants::facingMap[dir][1]))
		{
			x += Constants::facingMap[dir][0];
			y += Constants::facingMap[dir][1];
			if (caveMap.isOutOfBounds(x, y))
				break;
		}
		int skip = dist.roll() + dist.roll() + 5;
		for (int i = 0; i < skip; i++)
		{
			int oldX = x;
			int oldY = y;
			caveMap.followWall(x, y, dir);
		}
		int amount = dist.roll() / 2 + 2;
		for (int i = 0; i < amount; i++)
		{
			int oldX = x;
			int oldY = y;
			caveMap.caveMap[x + y * caveMap.w] = std::byte(Constants::BlockFuelCrystal);
			caveMap.followWall(x, y, dir);
		}
	}
}

void CaveGenerator::smooth(CaveChunk &caveMap)
{
	for(int column=0, row=0; row <= caveMap.h-1; row++)
	{
		for(column = 0; column <= caveMap.w-1; column++)
		{
			caveMap.caveMap[column + row * caveMap.w] = placeWallLogic(caveMap, column,row);
		}
	}
}

void CaveGenerator::randomFill(CaveChunk &caveMap)
{
	RandomUtil dist100(1, 100);

	int mapMiddle = caveMap.h / 2; // Temp variable
	for(int column=0,row=0; row < caveMap.h; row++)
	{
		for(column = 0; column < caveMap.w; column++)
		{
			// If coordinants lie on the the edge of the map (creates a border)
			if(column == 0)
			{
				caveMap.caveMap[column + row * caveMap.w] = std::byte(Constants::BlockWall);
			}
			else if (row == 0)
			{
				caveMap.caveMap[column + row * caveMap.w] = std::byte(Constants::BlockWall);
			}
			else if (column == caveMap.w-1)
			{
				caveMap.caveMap[column + row * caveMap.w] = std::byte(Constants::BlockWall);
			}
			else if (row == caveMap.h-1)
			{
				caveMap.caveMap[column + row * caveMap.w] = std::byte(Constants::BlockWall);
			}
			// Else, fill with a wall a random percent of the time
			else
			{
				if (row == mapMiddle)
				{
					caveMap.caveMap[column + row * caveMap.w] = std::byte(0);
				}
				else
				{
					if (dist100.roll() <= percentAreWalls)
						caveMap.caveMap[column + row * caveMap.w] = std::byte(Constants::BlockWall);
					else
						caveMap.caveMap[column + row * caveMap.w] = std::byte(0);
				}
			}
		}
	}
}

void CaveGenerator::print(CaveChunk &caveMap)
{
	for(int column=0,row=0; row < caveMap.h; row++ )
	{
		for( column = 0; column < caveMap.w; column++ )
		{
			if (caveMap.caveMap[column + row * caveMap.w] == std::byte(1))
				std::printf("#");
			else if (caveMap.caveMap[column + row * caveMap.w] == std::byte(2))
				std::printf("%%");
			else
				std::printf(".");
		}
		std::printf("\n");
	}
}

void CaveGenerator::addUnbreakable(CaveChunk &caveMap, bool north, bool east, bool south, bool west)
{
	if (north || south)
	{
		for (int i = 0; i < caveMap.w; i++)
		{
			if (north)
			{
				caveMap.caveMap[i] = std::byte(Constants::BlockUnbreakable);
			}
			if (south)
			{
				caveMap.caveMap[i + (caveMap.h - 1) * caveMap.w] = std::byte(Constants::BlockUnbreakable);
			}
		}
	}
	if (west || east)
	{
		for (int i = 0; i < caveMap.h; i++)
		{
			if (west)
			{
				caveMap.caveMap[i * caveMap.w] = std::byte(Constants::BlockUnbreakable);
			}
			if (east)
			{
				caveMap.caveMap[(caveMap.w - 1) + i * caveMap.w] = std::byte(Constants::BlockUnbreakable);
			}
		}
	}
}

std::byte CaveGenerator::placeWallLogic(CaveChunk &caveMap, int x,int y)
{
	int numWalls = caveMap.getAdjacentWalls(x,y,1,1);


	if(caveMap.caveMap[x + y * caveMap.w]==std::byte(Constants::BlockWall))
	{
		if (numWalls >= 4 )
		{
			return std::byte(1);
		}
		if (numWalls<2)
		{
			return std::byte(0);
		}
	}
	else
	{
		if (numWalls>=5)
		{
			return std::byte(1);
		}
	}
	return std::byte(0);
}
