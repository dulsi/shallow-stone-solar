#include "game.h"
#include "message.h"
#include "randomutil.h"
#include "pathfind/pathfind.h"

const int Game::startChunkX = Constants::ChunkAmountWidth / 2;
const int Game::startChunkY = Constants::ChunkAmountWidth / 2;

Game::Game()
: nId(1), spawnChance(5)
{
//	world.emplace(startChunkX+startChunkY*Constants::ChunkAmountWidth,CaveChunk());
	c.randomFill(world[startChunkX+startChunkY*Constants::ChunkAmountWidth]);
	c.smooth(world[startChunkX+startChunkY*Constants::ChunkAmountWidth]);
	c.addMinerals(world[startChunkX+startChunkY*Constants::ChunkAmountWidth]);
	enemies.reserve(1024);
}

void Game::addListener(GameListener *l)
{
	listener = l;
}

void Game::runTurn()
{
	int totalFuelCrystals = 0;
	for (auto &p : players)
	{
		totalFuelCrystals += p.fuelCrystals;
	}
	for (auto &p : players)
	{
		if (p.readyToMove())
		{
			if (totalFuelCrystals >= 50)
			{
				for (auto &e: enemies)
				{
					if (e.type == Constants::EnemyDropPod)
					{
						if ((abs(p.x - e.x) <= 1) && (abs(p.y - e.y) <= 1))
						{
							p.x = e.x;
							p.y = e.y;
							p.effect = Constants::PlayerEffectInPod;
							p.changed = true;
						}
					}
				}
			}
			if (p.effect != Constants::PlayerEffectInPod)
				p.move(*this);
		}
		if (p.control->lookXDir == 0 && p.control->lookYDir == 0)
		{
			if (p.control->xDir != 0 || p.control->yDir != 0)
				p.calcFacing(p.control->xDir, p.control->yDir);
		}
		else
		{
			p.calcFacing(p.control->lookXDir, p.control->lookYDir);
		}
		if (p.readyToAction())
		{
			p.act(*this);
		}
	}
	for (auto &e : enemies)
	{
		if (e.readyToMove())
			e.move(*this);
		if (e.readyToAction())
			e.act(*this);
	}
	for (auto &p : players)
	{
		if (p.changed)
		{
			listener->playerStatus(p);
			p.changed = false;
		}
	}
	auto e = enemies.begin();
	while (e != enemies.end())
	{
		if (e->changed)
		{
			listener->enemyStatus(*e);
			e->changed = false;
		}
		if (e->type != Constants::EnemyDropPod && e->health == 0)
		{
			e = enemies.erase(e);
		}
		else
			e++;
	}
	RandomUtil r(1, 1000);
	if (players.size() > 0 && r.roll() < spawnChance)
	{
		int nearPlayer = 0;
		if (players.size() > 1)
		{
			RandomUtil r(0, players.size() - 1);
			nearPlayer = r.roll();
		}
		int chunkX = players[nearPlayer].x / CaveChunk::w;
		int chunkY = players[nearPlayer].y / CaveChunk::h;
		RandomUtil r(1, 5);
		RandomUtil xRoll(0, CaveChunk::w - 1);
		RandomUtil yRoll(0, CaveChunk::h - 1);
		for (int num = r.roll(); num > 0; num--)
		{
			for (int i = 0; i < 10; i++)
			{
				int testX = chunkX * CaveChunk::w + xRoll.roll();
				int testY = chunkY * CaveChunk::h + yRoll.roll();
				if (isPassable(testX, testY))
				{
					Enemy e(nextId(), Constants::EnemyAnt, testX, testY);
					enemies.push_back(e);
					listener->enemyStatus(e);
					break;
				}
			}
		}
	}
}

std::shared_ptr<Controller> Game::getController(const std::string &name)
{
	for (auto &p : players)
	{
		if (p.name == name)
			return p.control;
	}
	Player p(name, findStartPosition());
	players.push_back(p);
	listener->playerStatus(p);
	Enemy e(nextId(), Constants::EnemyDropPod, p.x, p.y);
	enemies.push_back(e);
	listener->enemyStatus(e);
	listener->effect(Constants::EffectDropPod, p.x, p.y, 0, 0);
	return p.control;
}

bool Game::isPassable(int x, int y)
{
	int chunkX = x / CaveChunk::w;
	int chunkY = y / CaveChunk::h;
	if ((chunkX < 0) || (chunkY < 0) || (chunkX >= Constants::ChunkAmountWidth) || (chunkY >= Constants::ChunkAmountHeight))
		return false;
	auto chunk = world.find(chunkX + chunkY * Constants::ChunkAmountWidth);
	int val = 0;
	if (chunk != world.end())
	{
		if (chunk->second.isWall(x % CaveChunk::w, y % CaveChunk::h))
			return false;
	}
	for (auto &p : players)
	{
		if (x == p.x && y == p.y)
			return false;
	}
	for (auto &e : enemies)
	{
		if (x == e.x && y == e.y && e.type != Constants::EnemyGlowStick)
			return false;
	}
	return true;
}

std::byte Game::getWeight(int x, int y)
{
	int chunkX = x / CaveChunk::w;
	int chunkY = y / CaveChunk::h;
	if ((chunkX < 0) || (chunkY < 0) || (chunkX >= Constants::ChunkAmountWidth) || (chunkY >= Constants::ChunkAmountHeight))
		return maxWeight;
	auto chunk = world.find(chunkX + chunkY * Constants::ChunkAmountWidth);
	int val = 0;
	if (chunk != world.end())
	{
		if (chunk->second.isWall(x % CaveChunk::w, y % CaveChunk::h))
			return maxWeight;
		return std::byte(1);
	}
	else
		return maxWeight;
}

bool Game::mine(int x, int y, Player *p)
{
	int chunkX = x / CaveChunk::w;
	int chunkY = y / CaveChunk::h;
	if ((chunkX < 0) || (chunkY < 0) || (chunkX >= Constants::ChunkAmountWidth) || (chunkY >= Constants::ChunkAmountHeight))
		return false;
	auto chunk = world.find(chunkX + chunkY * Constants::ChunkAmountWidth);
	if (chunk != world.end())
	{
		auto w = chunk->second.caveMap[x % CaveChunk::w + (y % CaveChunk::h) * CaveChunk::w];
		if (w == std::byte(0))
			return false;
		MineRule *r = rules.getRule((int)w);
		if (r == NULL)
		{
			mapChange(chunkX, chunkY, chunk->second, x, y, int(w) - 1);
			listener->effect(Constants::EffectMine, x, y, 0, 0);
			return true;
		}
		else if (r->nextTile == -1)
			return false;
		else
		{
			mapChange(chunkX, chunkY, chunk->second, x, y, r->nextTile);
			listener->effect(Constants::EffectMine, x, y, 0, 0);
			if (p)
			{
				p->addFuelCrystals(r->fuelCrystals);
			}
			else
			{
				// drop fuel crystals
			}
		}
	}
	return false;
}

std::pair<int, int> Game::findStartPosition()
{
	int baseX = startChunkX * CaveChunk::w + CaveChunk::w / 2;
	int baseY = startChunkY * CaveChunk::h + CaveChunk::h / 2;
	int endY = CaveChunk::h / 2;
	int endX = CaveChunk::w / 2;
	for (int y = 0; y < endY; y++)
	{
		for (int x = 0; x < endX; x+= 2)
		{
			if (isPassable(baseX + x, baseY + y) && isPassable(baseX + x, baseY + y + 1) && isPassable(baseX + x + 1, baseY + y) && isPassable(baseX + x - 1, baseY + y))
				return std::pair(baseX + x, baseY + y);
			if (isPassable(baseX - x, baseY - y) && isPassable(baseX - x, baseY - y + 1) && isPassable(baseX - x + 1, baseY - y) && isPassable(baseX - x - 1, baseY - y))
				return std::pair(baseX - x, baseY - y);
			if (isPassable(baseX + x, baseY - y) && isPassable(baseX + x, baseY - y + 1) && isPassable(baseX + x + 1, baseY - y) && isPassable(baseX + x - 1, baseY - y))
				return std::pair(baseX + x, baseY - y);
			if (isPassable(baseX - x, baseY + y) && isPassable(baseX - x, baseY + y + 1) && isPassable(baseX - x + 1, baseY + y) && isPassable(baseX - x - 1, baseY + y))
				return std::pair(baseX - x, baseY + y);
		}
	}
	return std::pair(baseX, baseY);
}

void Game::mapChange(int chunkX, int chunkY, CaveChunk &chunk, int x, int y, int block)
{
	chunk.caveMap[x % CaveChunk::w + (y % CaveChunk::h) * CaveChunk::w] = std::byte(int(block));
	for (int i = 0; i < 8; i++)
	{
		int newChunkX = (x + Constants::facingMap[i][0]) / CaveChunk::w;
		int newChunkY = (y + Constants::facingMap[i][1]) / CaveChunk::h;
		if ((newChunkX == chunkX) && (newChunkY == chunkY))
			continue;
		if ((newChunkX < 0) || (newChunkY < 0) || (newChunkX >= Constants::ChunkAmountWidth) || (newChunkY >= Constants::ChunkAmountHeight))
			continue;
		auto chunk = world.find(newChunkX + newChunkY * Constants::ChunkAmountWidth);
		if (chunk == world.end())
		{
//			world.emplace(newChunkX+newChunkY*Constants::ChunkAmountWidth,CaveChunk());
			c.randomFill(world[newChunkX+newChunkY*Constants::ChunkAmountWidth]);
			c.smooth(world[newChunkX+newChunkY*Constants::ChunkAmountWidth]);
			c.addMinerals(world[newChunkX+newChunkY*Constants::ChunkAmountWidth]);
			c.addUnbreakable(world[newChunkX+newChunkY*Constants::ChunkAmountWidth], newChunkY == 0, newChunkX == Constants::ChunkAmountWidth - 1, newChunkY == Constants::ChunkAmountHeight - 1, newChunkX == 0);
			listener->addChunk(newChunkX, newChunkY, world[newChunkX+newChunkY*Constants::ChunkAmountWidth]);
		}
	}
	listener->mapChange(x, y, int(block));
}

int Game::nextId()
{
	return nId++;
}
