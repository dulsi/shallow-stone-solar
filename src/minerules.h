#ifndef __MINERULES_H
#define __MINERULES_H

#include <vector>

class MineRule
{
public:
	MineRule(int ct, int nt, int fc);

	int currentTile;
	int nextTile;
	int fuelCrystals;
};

class MineRules
{
public:
	MineRules();

	MineRule *getRule(int tile);

private:
	std::vector<MineRule> rules;
};

#endif
