#ifndef __PLAYER_H
#define __PLAYER_H

#include <SDL.h>
#include <SDL_net.h>
#include <memory>
#include <string>
#include "controller.h"
#include "playerstate.h"
#include "pathfind/pathfind.h"

class Game;

class Player : public PlayerState
{
public:
	Player(const std::string &n, std::pair<int, int> start);

	bool operator==(const Player &p) const;
	bool readyToMove();
	bool readyToAction();
	void move(Game &g);
	void act(Game &g);
	void calcFacing(int xDir, int yDir);
	int addFuelCrystals(int c);
	int addHealth(int h);
	int addMagazine(int b);

	bool changed;
	std::shared_ptr<Controller> control;
	PathFind path;

private:
	int moveCooldown;
	int actionCooldown;
	int effectDuration;
};

#endif
