#ifndef __CLIENTSTATE_H
#define __CLIENTSTATE_H

#include <unordered_map>
#include <memory>
#include "cavegenerator.h"
#include "clientvisibility.h"
#include "playerstate.h"
#include "enemystate.h"

class ClientState
{
public:
	ClientState();

	void clear();
	void setPlayer(const std::string &n);
	std::pair<int, int> getPosition();
	PlayerState *getPlayer();
	void addChunk(char *message);
	void updatePlayer(char *message);
	void updateEnemy(char *message);
	void mapChange(char *message);
	int getTile(int x, int y);
	void updateSight();

	Visibility sight;
	std::vector<PlayerState> players;
	std::vector<EnemyState> enemies;

private:
	std::unordered_map<int, CaveChunk> world;
	std::string name;
};

#endif
