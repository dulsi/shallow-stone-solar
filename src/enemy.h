#ifndef __ENEMY_H
#define __ENEMY_H

#include "enemystate.h"

class Game;

class Enemy : public EnemyState
{
public:
	Enemy(int ident, int t, int xPos, int yPos);

	bool readyToMove();
	bool readyToAction();
	void move(Game &g);
	void act(Game &g);
	int addHealth(int h);

	bool changed;
	int target;

private:
	int moveCooldown;
};

#endif
