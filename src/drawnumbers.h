#ifndef __DRAWNUMBERS_H
#define __DRAWNUMBERS_H

#include <SDL.h>
#include <SDL_ttf.h>
#include <vector>

class DrawNumbers
{
public:
	DrawNumbers(SDL_Renderer *r, TTF_Font *ttffont);

	void render(SDL_Renderer *r, int x, int y, int num);

private:
	SDL_Texture *numbers;
	std::vector<std::pair<int,int>> numberSize;
	std::vector<int> where;
};

#endif
