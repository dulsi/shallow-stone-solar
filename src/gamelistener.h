#ifndef __GAMELISTENER_H
#define __GAMELISTENER_H

#include "player.h"
#include "enemy.h"

class GameListener
{
public:
	virtual void addChunk(int x, int y, CaveChunk &chunk) = 0;
	virtual void playerStatus(Player &p) = 0;
	virtual void enemyStatus(Enemy &p) = 0;
	virtual void mapChange(int x, int y, int val) = 0;
	virtual void effect(int type, int x1, int y1, int x2, int y2) = 0;
};

#endif
