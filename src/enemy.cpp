#include "enemy.h"
#include "game.h"
#include "message.h"

Enemy::Enemy(int ident, int t, int xPos, int yPos)
: EnemyState(ident, t, xPos, yPos), changed(true), target(-1), moveCooldown(Constants::SpawnCooldownAnt)
{
	if (type == Constants::EnemyGlowStick)
		moveCooldown = 0;
}

bool Enemy::readyToMove()
{
	if (moveCooldown > 0)
		moveCooldown--;
	if (sprite == 1 && moveCooldown == 0)
		sprite = 0;
	return (moveCooldown == 0 && health > 0);
}

bool Enemy::readyToAction()
{
/*	if (actionCooldown > 0)
		actionCooldown--;
	return actionCooldown == 0;*/
	return (moveCooldown == 0 && health > 0);
}

void Enemy::move(Game &g)
{
	if (type == Constants::EnemyAnt)
	{
		if (g.players.size() > 0)
		{
			if (target == -1)
			{
				int i = 0;
				int currentDistance = 35;
				for (auto &p : g.players)
				{
					if (p.effect == Constants::PlayerEffectInPod)
						continue;
					int distance = abs(p.x - x);
					if (distance < abs(p.y - y))
						distance = abs(p.y - y);
					if ((distance <= 30) && (currentDistance > distance))
					{
						target = i;
						currentDistance = distance;
					}
					i++;
				}
			}
			if ((target != -1) && (g.players[target].effect != Constants::PlayerEffectInPod))
			{
				int distance = abs(g.players[target].x - x);
				if (distance < abs(g.players[target].y - y))
					distance = abs(g.players[target].y - y);
				if (distance <= 30)
				{
					if (distance != 1)
					{
						auto ans = g.players[target].path.path(g, x, y);
						if (x != ans.first || y != ans.second)
						{
							x = ans.first;
							y = ans.second;
							moveCooldown = Constants::MoveCooldownAnt;
							changed = true;
						}
					}
				}
				else
					target = -1;
			}
		}
	}
	else if (type == Constants::EnemyGlowStick)
	{
		if ((health > 194) && (target != 8))
		{
			if (MapWeight::maxWeight != g.getWeight(x + Constants::facingMap[target][0], y + Constants::facingMap[target][1]))
			{
				x += Constants::facingMap[target][0];
				y += Constants::facingMap[target][1];
				if (sprite == 2)
					sprite = 3;
				else
					sprite = 2;
			}
		}
	}
}

void Enemy::act(Game &g)
{
	switch (type)
	{
		case Constants::EnemyAnt:
			if (target != -1)
			{
				int distance = abs(g.players[target].x - x);
				if (distance < abs(g.players[target].y - y))
					distance = abs(g.players[target].y - y);
				if (distance == 1)
				{
					g.players[target].addHealth(-Constants::biteDamageAnt);
					moveCooldown = Constants::BiteCooldownAnt;
				}
			}
			break;
		case Constants::EnemyGlowStick:
			addHealth(-1);
			break;
		default:
			break;
	}
}

int Enemy::addHealth(int h)
{
	changed = true;
	health += h;
	if (health < 0)
		health = 0;
	return 0;
}
