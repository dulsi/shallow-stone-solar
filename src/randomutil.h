#ifndef __RANDOMUTILITY_H
#define __RANDOMUTILITY_H

class RandomUtil
{
public:
	RandomUtil(int low, int high);
	~RandomUtil();

	int roll();

private:
	void *impl;
};

#endif
