#ifndef __ENEMYSTATE_H
#define __ENEMYSTATE_H

class EnemyState
{
public:
	EnemyState(int ident, int t, int xPos, int yPos);
	EnemyState(int ident, char *message);
	EnemyState() {}

	void readMessage(char *message);
	void writeMessage(char *message);

	static int messageSize;

	int id;
	int type;
	int x, y;
	int health;
	int sprite;
};

#endif
