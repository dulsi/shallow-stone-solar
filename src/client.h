#ifndef __CLIENT_H
#define __CLIENT_H

#include <SDL.h>
#include <SDL_mixer.h>
#include <SDL_net.h>
#include <SDL_ttf.h>
#include <thread>
#include <vector>
#include "clientstate.h"
#include "drawnumbers.h"
#include "server.h"
#include "uitextinput.h"
#include "uimenu.h"
#include "uikeyselect.h"

class ClientConfig
{
public:
	ClientConfig();

	std::string name;
	std::string server;
};

class ClientController
{
public:
	ClientController();

	void process(SDL_Event &event);

	int yDir;
	int xDir;
	int lookYDir;
	int lookXDir;
	int l2Button;
	int r2Button;
	int xButton;
	int squareButton;
	int circleButton;
	int triangleButton;
	int optionButton;

	int axisHorizontal;
	int axisVertical;
	int axisLookHorizontal;
	int axisLookVertical;
	int axisMining;
	int axisShoot;

	SDL_Keycode keyLeft;
	SDL_Keycode keyRight;
	SDL_Keycode keyUp;
	SDL_Keycode keyDown;
	SDL_Keycode keyLookLeft;
	SDL_Keycode keyLookRight;
	SDL_Keycode keyLookUp;
	SDL_Keycode keyLookDown;
	SDL_Keycode keyL2;
	SDL_Keycode keyR2;
	SDL_Keycode keyX;
	SDL_Keycode keySquare;
	SDL_Keycode keyCircle;
	SDL_Keycode keyTriangle;
	SDL_Keycode keyOption;
	bool keyDefault;
};

class Client;

class Effect
{
public:
	Effect(char *message);

	bool draw(SDL_Renderer *r, Client *c, int centerX, int centerY, Uint64 ticks);

	int type;
	int x1, y1, x2, y2;
	Uint64 startTime;
	bool sound;
};

class Client
{
public:
	Client();
	void run();
	Mix_Music *getSound(int s);

private:
	void draw();
	void drawUI();
	void processMessage();
	std::pair<int, int> findTarget();
	void runMenu();
	void drawMenu(const std::string &errorMsg);
	void process(SDL_Event &event);

	SDL_Window *mainWindow;
	SDL_Renderer *mainRenderer;
	bool fullscreen;
	SDL_GameController *joystick;
	SDL_Texture *tileset;
	SDL_Texture *uiset;
	SDL_Texture *playerset;
	SDL_Texture *enemyset;
	SDL_Texture *droppodset;
	SDL_Texture *nametext;
	int nameWidth, nameHeight;
	SDL_Texture *fuelcrystaltext;
	int fuelcrystalWidth, fuelcrystalHeight;
	SDL_Texture *missiontext;
	int missionWidth, missionHeight;
	SDL_Texture *completetext;
	int completeWidth, completeHeight;
	SDL_Texture *wontext;
	int wonWidth, wonHeight;
	ClientConfig config;
	TCPsocket server;
	ClientState state;
	std::vector<char> buffer;
	ClientController control;
	int action;
	int targetId;
	TTF_Font *ttffont;
	std::unique_ptr<DrawNumbers> numbers;
	std::vector<Effect> effects;
	Mix_Music *sound[4];
	std::vector<UIMenuItem> startItem;
	std::vector<UIMenuItem> pauseItem;
	std::unique_ptr<UIMenu> menu;
	std::unique_ptr<UITextInput> textInput;
	std::unique_ptr<UIKeySelect> keySelect;
	std::unique_ptr<Server> serverObj;
	std::unique_ptr<std::thread> serverThread;
};

#endif
