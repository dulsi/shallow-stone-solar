#ifndef __UITEXTINPUT_H
#define __UITEXTINPUT_H
#include <SDL.h>
#include <SDL_ttf.h>
#include <string>

class ClientController;

class UITextInput
{
public:
	UITextInput(SDL_Renderer *r, TTF_Font *font, int x1, int y1, const std::string &initial);
	~UITextInput();

	int process(ClientController &control);
	void draw(SDL_Renderer *r);

	std::string text;

private:
	int x;
	int y;
	int shift;
	int pX;
	int pY;
	SDL_Texture *keyTex[2];
	std::string prevtext;
	SDL_Texture *currentTex;
	int cW;
	int cH;
	TTF_Font *f;
	int moveCooldown;
	int actionCooldown;
};

#endif
