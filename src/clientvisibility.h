#ifndef __CLIENTVISIBILITY_H
#define __CLIENTVISIBILITY_H

#include <unordered_map>
#include <vector>
#include "cavegenerator.h"
#include "permissive-fov/permissive-fov-cpp.h"

class ClientState;

class VisibilityMask
{
public:
	VisibilityMask(ClientState *s);

	bool show(int x, int y);
	void clear();

	bool isBlocked(short destX, short destY);
	void visit(short destX, short destY);

private:
	ClientState *state;
	std::unordered_map<int, bool> mask;
};

class Visibility
{
public:
	Visibility(ClientState *s);

	enum Visible {Never, Past, Present};

	Visible canSee(int x, int y);
	void clearAll();
	void clearNow();
	void calculate(int x, int y);

	bool isBlocked(short destX, short destY);
	void visit(short destX, short destY);

private:
	ClientState *state;
	VisibilityMask squaremask;
	permissive::maskT playermask;
	permissive::maskT glowstickmask;
	std::unordered_map<int, std::vector<bool>> seenBefore;
	std::unordered_map<int, std::vector<bool>> seenNow;
};

#endif
