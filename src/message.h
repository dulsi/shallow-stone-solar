#ifndef __MESSAGE_H
#define __MESSAGE_H

class Message
{
public:
	static const int NewChunk = 1;
	static const int PlayerStatus = 2;
	static const int MovePlayer = 3;
	static const int LookPlayer = 4;
	static const int ActionPlayer = 5;
	static const int MapChange = 6;
	static const int EnemyStatus = 7;
	static const int Effect = 8;
	static const int Shutdown = 9;
};

class Constants
{
public:
	static const int ActionNone = 0;
	static const int ActionMine = 1;
	static const int ActionShoot = 2;
	static const int ActionThrowGlowsitck = 3;

	static const int MineCooldown = 4;
	static const int GunCooldown = 3;
	static constexpr int facingMap[8][2] = {
		{ 0, -1},
		{ 1, -1},
		{ 1,  0},
		{ 1,  1},
		{ 0,  1},
		{-1,  1},
		{-1,  0},
		{-1, -1}
	};

	static const int BlockWall = 1;
	static const int BlockFuelCrystal = 2;
	static const int BlockUnbreakable = 3;
	static const int BlockShadow = 4;

	static const int EnemyDropPod = 0;
	static const int EnemyGlowStick = 1;
	static const int EnemyFirstHostile = 2;
	static const int EnemyAnt = 2;

	static const int SpawnCooldownAnt = 12;
	static const int MoveCooldownAnt = 4;
	static const int BiteCooldownAnt = 6;

	static const int biteDamageAnt = 4;

	static const int gunDamage = 10;
	static const int MagazineSize = 25;

	static const int ChunkAmountWidth = 32;
	static const int ChunkAmountHeight = 32;

	static const int EffectShot = 1;
	static const int EffectMine = 2;
	static const int EffectDropPod = 3;
	static const int EffectReload = 4;

	static const int SoundShot = 0;
	static const int SoundMine = 1;
	static const int SoundDropPod = 2;
	static const int SoundReload = 3;

	static const int PlayerEffectInPod = 1;
};

#endif
