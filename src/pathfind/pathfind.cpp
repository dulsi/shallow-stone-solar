#include "pathfind.h"
#include "../message.h"
#include <cstdio>

PathItem::PathItem(int w, int xPos, int yPos)
: weight(w), x(xPos), y(yPos)
{
}

PathFind::PathFind()
{
}

void PathFind::clear()
{
	while (!unfinished.empty())
		unfinished.pop();
	while (!weighted.empty())
		weighted.pop();
	for (int i = 0; i < 61*61; i++)
	{
		map[i] = std::byte(0);
	}
	unfinished.push(PathItem(1, 30,30));
}

void PathFind::setEndPoint(int x, int y)
{
	endX = x;
	endY = y;
	clear();
}

std::pair<int, int> PathFind::path(MapWeight &m, int startX, int startY)
{
	int pX = startX - endX + 30;
	int pY = startY - endY + 30;
	if (pX < 0 || pX >=61 || pY < 0 || pY >= 61)
	{
		return std::pair(startX, startY);
	}
	while (!unfinished.empty() || !weighted.empty())
	{
		if (map[startX - endX + 30 + (startY - endY + 30) * 61] != std::byte(0))
			break;
		while (!weighted.empty() && unfinished.front().weight < weighted.front().weight)
		{
			process(m, weighted.front());
			weighted.pop();
		}
		if (!unfinished.empty())
		{
			process(m, unfinished.front());
			unfinished.pop();
		}
	}
/*	static bool once = false;
	if (!once)
	{
		for (int y1 = 0; y1 < 61; y1++)
		{
			for (int x1 = 0; x1 < 61; x1++)
				std::printf(" %-3d", map[x1 + y1 * 61]);
			std::printf("\n");
		}
		once = true;
	}*/
	if (map[startX - endX + 30 + (startY - endY + 30) * 61] != std::byte(0))
	{
		std::byte lowest = MapWeight::maxWeight;
		std::vector<std::pair<int, int>> possible;
		for (int i = 0; i < 8; i++)
		{
			std::byte weight = map[startX - endX + 30 + Constants::facingMap[i][0] + (startY - endY + 30 + Constants::facingMap[i][1]) * 61];
			if ((weight != MapWeight::maxWeight) && (weight != std::byte(0)) && (m.isPassable(startX + Constants::facingMap[i][0], startY + Constants::facingMap[i][1])))
			{
				if (weight < lowest)
					lowest = weight;
				possible.push_back(std::pair(startX + Constants::facingMap[i][0], startY + Constants::facingMap[i][1]));
			}
		}
		for (auto &p : possible)
		{
			if (lowest == map[p.first - endX + 30 + (p.second - endY + 30) * 61])
				return p;
		}
	}
	return std::pair(startX, startY);
}

void PathFind::process(MapWeight &m, const PathItem &p)
{
	if (p.x < 0 || p.x >=61 || p.y < 0 || p.y >= 61)
		return;
	if (map[p.x + p.y * 61] != std::byte(0))
		return;
	map[p.x + p.y * 61] = std::byte(p.weight);
	for (int i = 0; i < 8; i++)
	{
		int newX = p.x + Constants::facingMap[i][0];
		int newY = p.y + Constants::facingMap[i][1];
		if (newX < 0 || newX >=61 || newY < 0 || newY >= 61)
			continue;
		if (map[newX + newY * 61] != std::byte(0))
			continue;
		int newWeight = int(m.getWeight(newX + endX - 30, newY + endY - 30));
		if ((newWeight == int(m.maxWeight)) || (int(m.maxWeight) <= p.weight + newWeight))
			map[newX + newY * 61] = MapWeight::maxWeight;
		else
		{
			if (newWeight == 1)
				unfinished.push(PathItem(newWeight + p.weight, newX, newY));
			else
				weighted.push(PathItem(newWeight + p.weight, newX, newY));
		}
	}
}
