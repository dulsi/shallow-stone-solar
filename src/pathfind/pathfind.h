#ifndef __PATHFIND_H
#define __PATHFIND_H

#include <queue>

class MapWeight
{
public:
	virtual bool isPassable(int x, int y) = 0;
	virtual std::byte getWeight(int x, int y) = 0;

	static const std::byte maxWeight = std::byte(255);
};

class PathItem
{
public:
	PathItem(int w, int xPos, int yPos);

	int weight;
	int x;
	int y;
};

class PathFind
{
public:
	PathFind();

	void clear();
	void setEndPoint(int x, int y);
	std::pair<int, int> path(MapWeight &m, int startX, int startY);

private:
	void process(MapWeight &m, const PathItem &p);

	int endX, endY;
	std::byte map[61*61];
	std::queue<PathItem> unfinished;
	std::queue<PathItem> weighted;
};

#endif
