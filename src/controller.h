#ifndef __CONTROLLER_H
#define __CONTROLLER_H

#include <SDL.h>
#include <SDL_net.h>

class Controller
{
public:
	Controller();

	bool processMessages(int len, char *message);

	int yDir;
	int xDir;
	int lookYDir;
	int lookXDir;
	int action;
	int targetId;
};

#endif
