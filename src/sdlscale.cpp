#include <SDL.h>
#include "sdlscale.h"

static double scalew = 1;
static double scaleh = 1;

void SDLScale_RenderCopy(SDL_Renderer *renderer, SDL_Texture *texture, const SDL_Rect *srcrect, const SDL_Rect *dstrect)
{
	SDL_Rect dstreal;
	if (dstrect) {
		dstreal.x = dstrect->x * scalew;
		dstreal.y = dstrect->y * scaleh;
		dstreal.h = dstrect->h * scaleh;
		dstreal.w = dstrect->w * scalew;
		SDL_RenderCopy(renderer, texture, srcrect, &dstreal);
	}
	else
		SDL_RenderCopy(renderer, texture, srcrect, dstrect);
}

void SDLScale_RenderDrawLine(SDL_Renderer *renderer, int x1, int y1, int x2, int y2)
{
	SDL_RenderDrawLine(renderer, x1 * scalew, y1 * scaleh, x2 * scalew, y2 * scaleh);
}

void SDLScale_RenderDrawRect(SDL_Renderer *renderer, SDL_Rect *dstrect)
{
	SDL_Rect dstreal;
	dstreal.x = dstrect->x * scalew;
	dstreal.y = dstrect->y * scaleh;
	dstreal.h = dstrect->h * scaleh;
	dstreal.w = dstrect->w * scalew;
	SDL_RenderDrawRect(renderer, &dstreal);
}

void SDLScale_Set(double w, double h)
{
	scalew = w;
	scaleh = h;
}
