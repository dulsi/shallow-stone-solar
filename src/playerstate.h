#ifndef __PLAYERSTATE_H
#define __PLAYERSTATE_H

#include <string>

class PlayerState
{
public:
	PlayerState(const std::string &n, int xPos, int yPos, int f);
	PlayerState(const std::string &n, char *message);
	PlayerState() {}

	void readMessage(char *message);
	void writeMessage(char *message);

	static int messageSize;

	std::string name;
	int x;
	int y;
	int facing;
	int health;
	int fuelCrystals;
	int effect;
	int magazine;
};

#endif
