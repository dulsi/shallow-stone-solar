#include <SDL.h>
#include <SDL_net.h>
#include <cstdio>
#include "clientstate.h"
#include "message.h"

ClientState::ClientState()
: sight(this)
{
}

void ClientState::clear()
{
	name = "";
	players.resize(0);
	enemies.resize(0);
	world.clear();
	sight.clearAll();
}

void ClientState::setPlayer(const std::string &n)
{
	name = n;
}

std::pair<int, int> ClientState::getPosition()
{
	PlayerState *p = getPlayer();
	if (p)
	{
		return std::pair(p->x, p->y);
	}
	return std::pair(0, 0);
}

PlayerState *ClientState::getPlayer()
{
	for (auto &p : players)
	{
		if (name == p.name)
		{
			return &p;
		}
	}
	return NULL;
}

void ClientState::addChunk(char *message)
{
	int x = SDLNet_Read16(message);
	int y = SDLNet_Read16(message+2);
	world.emplace(x+y*Constants::ChunkAmountWidth,CaveChunk());
	memcpy(&world[x+y*Constants::ChunkAmountWidth].caveMap[0], message + 4, CaveChunk::w * CaveChunk::h);
}

void ClientState::updatePlayer(char *message)
{
	message[20] = '\0';
	std::string n(message);
	bool found = false;
	for (auto &p : players)
	{
		if (p.name == n)
		{
			p.readMessage(message);
			found = true;
			break;
		}
	}
	if (!found)
	{
		players.push_back(PlayerState(n, message));
	}
}

void ClientState::updateEnemy(char *message)
{
	int id = SDLNet_Read16(message);
	bool found = false;
	for (auto &e : enemies)
	{
		if (e.id == id)
		{
			e.readMessage(message);
			found = true;
			break;
		}
	}
	if (!found)
	{
		enemies.push_back(EnemyState(id, message));
	}
}

void ClientState::mapChange(char *message)
{
	int x = SDLNet_Read16(message);
	int y = SDLNet_Read16(message + 2);
	int wall = SDLNet_Read16(message + 4);
	int chunkX = x / CaveChunk::w;
	int chunkY = y / CaveChunk::h;
	auto c = world.find(chunkX + chunkY * Constants::ChunkAmountWidth);
	if (c != world.end())
	{
		c->second.caveMap[x % CaveChunk::w + (y % CaveChunk::h) * CaveChunk::w] = std::byte(wall);
	}
}

int ClientState::getTile(int x, int y)
{
	int chunkX = x / CaveChunk::w;
	int chunkY = y / CaveChunk::h;
	if ((chunkX < 0) || (chunkY < 0) || (chunkX >= Constants::ChunkAmountWidth) || (chunkY >= Constants::ChunkAmountHeight))
		return 1;
	auto c = world.find(chunkX + chunkY * Constants::ChunkAmountWidth);
	int val = 0;
	if (c != world.end())
	{
		val = (int)c->second.caveMap[x % CaveChunk::w + (y % CaveChunk::h) * CaveChunk::w];
	}
	return val;
}

void ClientState::updateSight()
{
	for (auto &p : players)
	{
		if (p.name == name)
		{
			sight.calculate(p.x, p.y);
			break;
		}
	}
}
