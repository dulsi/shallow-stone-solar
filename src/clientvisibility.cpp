#include "clientvisibility.h"
#include "clientstate.h"
#include "message.h"


VisibilityMask::VisibilityMask(ClientState *s)
: state(s)
{
}

bool VisibilityMask::show(int x, int y)
{
	auto c = mask.find(x + y * CaveChunk::w);
	if (c != mask.end())
		return c->second;
	return false;
}

void VisibilityMask::clear()
{
	mask.clear();
}

bool VisibilityMask::isBlocked(short destX, short destY)
{
	return 0 != state->getTile(destX, destY);
}

void VisibilityMask::visit(short destX, short destY)
{
	mask[destX + destY * CaveChunk::w] = true;
}

Visibility::Visibility(ClientState *s)
: state(s), squaremask(s), playermask("data/player.mask"), glowstickmask("data/glowstick.mask")
{
}

Visibility::Visible Visibility::canSee(int x, int y)
{
	int chunkX = x / CaveChunk::w;
	int chunkY = y / CaveChunk::h;
	if ((chunkX < 0) || (chunkY < 0) || (chunkX >= Constants::ChunkAmountWidth) || (chunkY >= Constants::ChunkAmountHeight))
		return Visibility::Never;
	auto c = seenNow.find(chunkX + chunkY * Constants::ChunkAmountWidth);
	if (c != seenNow.end())
	{
		if (c->second[x % CaveChunk::w + (y % CaveChunk::h) * CaveChunk::w])
			return Visibility::Present;
	}
	c = seenBefore.find(chunkX + chunkY * Constants::ChunkAmountHeight);
	if (c != seenBefore.end())
	{
		if (c->second[x % CaveChunk::w + (y % CaveChunk::h) * CaveChunk::w])
			return Visibility::Past;
	}
	return Visibility::Never;
}

void Visibility::clearAll()
{
	clearNow();
	seenBefore.clear();
}

void Visibility::clearNow()
{
	seenNow.clear();
}

void Visibility::calculate(int x, int y)
{
	clearNow();
	squaremask.clear();
	permissive::squareFov(x, y, 20, squaremask);
	permissive::fov(x, y, playermask, *this);
	for (auto &e : state->enemies)
	{
		if (e.type == Constants::EnemyGlowStick)
		{
			if (40 > abs(x - e.x) && 40 > abs(y - e.y))
				permissive::fov(e.x, e.y, glowstickmask, *this);
		}
	}
}

bool Visibility::isBlocked(short destX, short destY)
{
	return 0 != state->getTile(destX, destY);
}

void Visibility::visit(short destX, short destY)
{
	if (!squaremask.show(destX, destY))
		return;
	int chunkX = destX / CaveChunk::w;
	int chunkY = destY / CaveChunk::h;
	if ((chunkX < 0) || (chunkY < 0) || (chunkX >= Constants::ChunkAmountWidth) || (chunkY >= Constants::ChunkAmountHeight))
		return;
	auto c = seenNow.find(chunkX + chunkY * Constants::ChunkAmountWidth);
	if (c == seenNow.end())
	{
		seenNow.emplace(chunkX+chunkY*Constants::ChunkAmountWidth,std::vector<bool>(CaveChunk::w * CaveChunk::h, false));
	}
	c = seenBefore.find(chunkX + chunkY * Constants::ChunkAmountWidth);
	if (c == seenBefore.end())
	{
		seenBefore.emplace(chunkX+chunkY*Constants::ChunkAmountWidth,std::vector<bool>(CaveChunk::w * CaveChunk::h, false));
	}
	seenNow[chunkX + chunkY * Constants::ChunkAmountWidth][destX % CaveChunk::w + (destY % CaveChunk::h) * CaveChunk::w] = true;
	seenBefore[chunkX + chunkY * Constants::ChunkAmountWidth][destX % CaveChunk::w + (destY % CaveChunk::h) * CaveChunk::w] = true;
}
