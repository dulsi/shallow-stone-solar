#include <SDL_image.h>
#include <cstdio>
#include <cstring>
#include <thread>
#include "client.h"
#include "message.h"
#include "randomutil.h"
#include "sdlscale.h"

ClientConfig::ClientConfig()
: name("user"), server("localhost")
{
}

Effect::Effect(char *message)
: sound(false)
{
	type = SDLNet_Read16(message);
	x1 = SDLNet_Read16(message + 2);
	y1 = SDLNet_Read16(message + 4);
	x2 = SDLNet_Read16(message + 6);
	y2 = SDLNet_Read16(message + 8);
	startTime = SDL_GetTicks64();
}

bool Effect::draw(SDL_Renderer *r, Client *c, int centerX, int centerY, Uint64 ticks)
{
	switch(type)
	{
		case Constants::EffectShot:
		{
			if (!sound)
			{
				Mix_PlayMusic(c->getSound(Constants::SoundShot),1);
				sound = true;
			}
			int aX1 = (x1 - centerX + 15) * 32 + 16;
			int aY1 = (y1 - centerY + 8) * 32 + 16;
			int aX2 = (x2 - centerX + 15) * 32 + 16;
			int aY2 = (y2 - centerY + 8) * 32 + 16;
			if (ticks < startTime + 13)
			{
				aX2 = (aX2 + aX1) / 2;
				aY2 = (aY2 + aY1) / 2;
			}
			else
			{
				aX1 = (aX2 + aX1) / 2;
				aY1 = (aY2 + aY1) / 2;
			}
			if ((aX1 >= 0 && aX1 < 960 && aY1 >= 0 && aY1 < 540) || (aX2 >= 0 && aX2 < 960 && aY2 >= 0 && aY2 < 540))
			{
				SDL_SetRenderDrawColor(r, 255, 255, 255, 255);
				SDLScale_RenderDrawLine(r, aX1, aY1, aX2, aY2);
				SDL_SetRenderDrawColor(r, 0, 0, 0, 255);
			}
			if (ticks > startTime + 25)
				return true;
			else
				return false;
			break;
		}
		case Constants::EffectMine:
		{
			Mix_PlayMusic(c->getSound(Constants::SoundMine),1);
			return true;
		}
		case Constants::EffectDropPod:
		{
			if (!sound)
			{
				Mix_PlayMusic(c->getSound(Constants::SoundDropPod),1);
				sound = true;
			}
			int aX1 = (x1 - centerX + 15) * 32;
			int aY1 = (y1 - centerY + 8) * 32;
			int aX2 = (x1 - centerX + 15) * 32;
			int aY2 = (y1 - centerY + 8) * 32 - 100;
			if ((aX1 >= 0 && aX1 < 960 && aY1 >= 0 && aY1 < 540) || (aX2 >= 0 && aX2 < 960 && aY2 >= 0 && aY2 < 540))
			{
				SDL_SetRenderDrawColor(r, 255, 255, 255, 255);
				RandomUtil roll(0, 31);
				for (int i = 0; i < 8; i++)
				{
					int offset = roll.roll();
					SDLScale_RenderDrawLine(r, aX1 + offset, aY1, aX2 + offset, aY2);
				}
				SDL_SetRenderDrawColor(r, 0, 0, 0, 255);
			}
			if (ticks > startTime + 50)
				return true;
			else
				return false;
			break;
		}
		case Constants::EffectReload:
		{
			Mix_PlayMusic(c->getSound(Constants::SoundReload),1);
			return true;
		}
		default:
			return true;
			break;
	}
	return false;
}

ClientController::ClientController()
: xDir(0), yDir(0), lookXDir(0), lookYDir(0), l2Button(0), r2Button(0), xButton(0), squareButton(0), triangleButton(0), circleButton(0), optionButton(0), axisHorizontal(SDL_CONTROLLER_AXIS_LEFTX), axisVertical(SDL_CONTROLLER_AXIS_LEFTY), axisLookHorizontal(SDL_CONTROLLER_AXIS_RIGHTX), axisLookVertical(SDL_CONTROLLER_AXIS_RIGHTY), axisMining(SDL_CONTROLLER_AXIS_TRIGGERLEFT), axisShoot(SDL_CONTROLLER_AXIS_TRIGGERRIGHT)
{
	keyLeft = SDLK_LEFT;
	keyRight = SDLK_RIGHT;
	keyUp = SDLK_UP;
	keyDown = SDLK_DOWN;
	keyLookLeft = SDLK_a;
	keyLookRight = SDLK_d;
	keyLookUp = SDLK_w;
	keyLookDown = SDLK_s;
	keyL2 = SDLK_e;
	keyR2 = SDLK_SPACE;
	keyX = SDLK_RETURN;
	keySquare = SDLK_r;
	keyCircle = SDLK_SLASH;
	keyTriangle = SDLK_f;
	keyOption = SDLK_ESCAPE;
	keyDefault = true;
}

void ClientController::process(SDL_Event &sdlevent)
{
	if (sdlevent.type == SDL_CONTROLLERAXISMOTION)
	{
		if (sdlevent.caxis.value < -3200)
		{
			if (sdlevent.caxis.axis == axisHorizontal)
				xDir = 2;
			else if (sdlevent.caxis.axis == axisVertical)
				yDir = 2;
			else if (sdlevent.caxis.axis == axisLookHorizontal)
				lookXDir = 2;
			else if (sdlevent.caxis.axis == axisLookVertical)
				lookYDir = 2;
			else if (sdlevent.caxis.axis == axisMining)
				l2Button = 0;
			else if (sdlevent.caxis.axis == axisShoot)
				r2Button = 0;
		}
		else if (sdlevent.caxis.value > 3200)
		{
			if (sdlevent.caxis.axis == axisHorizontal)
				xDir = 1;
			else if (sdlevent.caxis.axis == axisVertical)
				yDir = 1;
			else if (sdlevent.caxis.axis == axisLookHorizontal)
				lookXDir = 1;
			else if (sdlevent.caxis.axis == axisLookVertical)
				lookYDir = 1;
			else if (sdlevent.caxis.axis == axisMining)
				l2Button = 1;
			else if (sdlevent.caxis.axis == axisShoot)
				r2Button = 1;
		}
		else
		{
			if (sdlevent.caxis.axis == axisHorizontal)
				xDir = 0;
			else if (sdlevent.caxis.axis == axisVertical)
				yDir = 0;
			else if (sdlevent.caxis.axis == axisLookHorizontal)
				lookXDir = 0;
			else if (sdlevent.caxis.axis == axisLookVertical)
				lookYDir = 0;
			else if (sdlevent.caxis.axis == axisMining)
				l2Button = 0;
			else if (sdlevent.caxis.axis == axisShoot)
				r2Button = 0;
		}
	}
	else if (sdlevent.type == SDL_CONTROLLERBUTTONDOWN)
	{
		if (sdlevent.cbutton.button == SDL_CONTROLLER_BUTTON_A)
		{
			xButton = 1;
		}
		else if (sdlevent.cbutton.button == SDL_CONTROLLER_BUTTON_X)
		{
			squareButton = 1;
		}
		else if (sdlevent.cbutton.button == SDL_CONTROLLER_BUTTON_B)
		{
			circleButton = 1;
		}
		else if (sdlevent.cbutton.button == SDL_CONTROLLER_BUTTON_Y)
		{
			triangleButton = 1;
		}
		else if (sdlevent.cbutton.button == SDL_CONTROLLER_BUTTON_START)
		{
			optionButton = 1;
		}
	}
	else if (sdlevent.type == SDL_CONTROLLERBUTTONUP)
	{
		if (sdlevent.cbutton.button == SDL_CONTROLLER_BUTTON_A)
		{
			xButton = 0;
		}
		else if (sdlevent.cbutton.button == SDL_CONTROLLER_BUTTON_X)
		{
			squareButton = 0;
		}
		else if (sdlevent.cbutton.button == SDL_CONTROLLER_BUTTON_B)
		{
			circleButton = 0;
		}
		else if (sdlevent.cbutton.button == SDL_CONTROLLER_BUTTON_Y)
		{
			triangleButton = 0;
		}
		else if (sdlevent.cbutton.button == SDL_CONTROLLER_BUTTON_START)
		{
			optionButton = 0;
		}
	}
	else if (sdlevent.type == SDL_KEYDOWN)
	{
		if ((sdlevent.key.keysym.sym == keyUp) || (keyDefault && (sdlevent.key.keysym.sym == SDLK_KP_8)))
			yDir = 2;
		else if ((sdlevent.key.keysym.sym == keyDown) || (keyDefault && (sdlevent.key.keysym.sym == SDLK_KP_2)))
			yDir = 1;
		else if ((sdlevent.key.keysym.sym == keyLeft) || (keyDefault && (sdlevent.key.keysym.sym == SDLK_KP_4)))
			xDir = 2;
		else if ((sdlevent.key.keysym.sym == keyRight) || (keyDefault && (sdlevent.key.keysym.sym == SDLK_KP_6)))
			xDir = 1;
		else if (sdlevent.key.keysym.sym == keyLookUp)
			lookYDir = 2;
		else if (sdlevent.key.keysym.sym == keyLookDown)
			lookYDir = 1;
		else if (sdlevent.key.keysym.sym == keyLookLeft)
			lookXDir = 2;
		else if (sdlevent.key.keysym.sym == keyLookRight)
			lookXDir = 1;
		else if (sdlevent.key.keysym.sym == keyR2)
			r2Button = 1;
		else if (sdlevent.key.keysym.sym == keyL2)
			l2Button = 1;
		else if (sdlevent.key.keysym.sym == keyX)
			xButton = 1;
		else if (sdlevent.key.keysym.sym == keyCircle)
			circleButton = 1;
		else if (sdlevent.key.keysym.sym == keySquare)
			squareButton = 1;
		else if (sdlevent.key.keysym.sym == keyTriangle)
			triangleButton = 1;
		else if (sdlevent.key.keysym.sym == keyOption)
			optionButton = 1;
	}
	else if (sdlevent.type == SDL_KEYUP)
	{
		if ((sdlevent.key.keysym.sym == keyUp) || (keyDefault && (sdlevent.key.keysym.sym == SDLK_KP_8)))
			yDir = 0;
		else if ((sdlevent.key.keysym.sym == keyDown) || (keyDefault && (sdlevent.key.keysym.sym == SDLK_KP_2)))
			yDir = 0;
		else if ((sdlevent.key.keysym.sym == keyLeft) || (keyDefault && (sdlevent.key.keysym.sym == SDLK_KP_4)))
			xDir = 0;
		else if ((sdlevent.key.keysym.sym == keyRight) || (keyDefault && (sdlevent.key.keysym.sym == SDLK_KP_6)))
			xDir = 0;
		else if ((sdlevent.key.keysym.sym == keyLookUp) && (lookYDir == 2))
			lookYDir = 0;
		else if ((sdlevent.key.keysym.sym == keyLookDown) && (lookYDir == 1))
			lookYDir = 0;
		else if ((sdlevent.key.keysym.sym == keyLookLeft) && (lookXDir == 2))
			lookXDir = 0;
		else if ((sdlevent.key.keysym.sym == keyLookRight) && (lookXDir == 1))
			lookXDir = 0;
		else if (sdlevent.key.keysym.sym == keyR2)
			r2Button = 0;
		else if (sdlevent.key.keysym.sym == keyL2)
			l2Button = 0;
		else if (sdlevent.key.keysym.sym == keyX)
			xButton = 0;
		else if (sdlevent.key.keysym.sym == keyCircle)
			circleButton = 0;
		else if (sdlevent.key.keysym.sym == keySquare)
			squareButton = 0;
		else if (sdlevent.key.keysym.sym == keyTriangle)
			triangleButton = 0;
		else if (sdlevent.key.keysym.sym == keyOption)
			optionButton = 0;
	}
}

Client::Client()
: action(Constants::ActionNone), targetId(-1), fullscreen(false)
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_AUDIO | SDL_INIT_GAMECONTROLLER) == -1) {
		std::printf("SDL_Init: %s\n", SDL_GetError());
		std::exit(1);
	}
	Mix_Init(MIX_INIT_FLAC | MIX_INIT_OGG);
	if (SDLNet_Init() == -1) {
		std::printf("SDLNet_Init: %s\n", SDLNet_GetError());
		std::exit(2);
	}
	SDL_GameControllerAddMappingsFromFile("data/gamecontrollerdb.txt");
	mainWindow = SDL_CreateWindow("Shallow Stone Solar",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		960,
		540,
		(false ? SDL_WINDOW_FULLSCREEN_DESKTOP : 0));
	if (mainWindow == NULL)
	{
		printf("Failed - SDL_CreateWindow\n");
		exit(0);
	}
	int xScreen, yScreen;
	SDL_GetWindowSize(mainWindow, &xScreen, &yScreen);
	SDLScale_Set(xScreen / 960, yScreen / 540);

	mainRenderer = SDL_CreateRenderer(mainWindow, -1, 0);
	if (mainRenderer == NULL)
	{
		printf("Failed - SDL_CreateRenderer\n");
		exit(0);
	}
	SDL_Surface *img = IMG_Load("data/tileset.png");
	tileset = SDL_CreateTextureFromSurface(mainRenderer, img);
	SDL_FreeSurface(img);
	img = IMG_Load("data/uiset.png");
	uiset = SDL_CreateTextureFromSurface(mainRenderer, img);
	SDL_FreeSurface(img);
	img = IMG_Load("data/player.png");
	playerset = SDL_CreateTextureFromSurface(mainRenderer, img);
	SDL_FreeSurface(img);
	img = IMG_Load("data/enemy.png");
	enemyset = SDL_CreateTextureFromSurface(mainRenderer, img);
	SDL_FreeSurface(img);
	img = IMG_Load("data/droppod.png");
	droppodset = SDL_CreateTextureFromSurface(mainRenderer, img);
	SDL_FreeSurface(img);
	SDL_SetRenderDrawColor(mainRenderer, 0, 0, 0, 255);
	SDL_RenderClear(mainRenderer);
	int maxJoysticks = SDL_NumJoysticks();
	for(int JoystickIndex=0; JoystickIndex < maxJoysticks; ++JoystickIndex)
	{
		if (!SDL_IsGameController(JoystickIndex))
		{
			continue;
		}
		joystick = SDL_GameControllerOpen(JoystickIndex);
	}
	if (TTF_Init() == -1)
	{
		printf("Failed - TTF_Init\n");
		exit(0);
	}
	ttffont = TTF_OpenFont("data/DejaVuSans.ttf", 16);
	SDL_Color c;
	c.a = 255;
	c.r = 255;
	c.g = 255;
	c.b = 255;
	img = TTF_RenderUTF8_Solid(ttffont, "Hydrogrossular", c);
	fuelcrystalWidth = img->w;
	fuelcrystalHeight = img->h;
	fuelcrystaltext = SDL_CreateTextureFromSurface(mainRenderer, img);
	SDL_FreeSurface(img);
	img = TTF_RenderUTF8_Solid(ttffont, "Extract Hydrogrossular           /", c);
	missionWidth = img->w;
	missionHeight = img->h;
	missiontext = SDL_CreateTextureFromSurface(mainRenderer, img);
	SDL_FreeSurface(img);
	img = TTF_RenderUTF8_Solid(ttffont, "Mission Complete! Return to drop pod.", c);
	completeWidth = img->w;
	completeHeight = img->h;
	completetext = SDL_CreateTextureFromSurface(mainRenderer, img);
	SDL_FreeSurface(img);
	img = TTF_RenderUTF8_Solid(ttffont, "Success! You Won!", c);
	wonWidth = img->w;
	wonHeight = img->h;
	wontext = SDL_CreateTextureFromSurface(mainRenderer, img);
	SDL_FreeSurface(img);
	numbers = std::make_unique<DrawNumbers>(mainRenderer, ttffont);
	Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 1024);
	sound[Constants::SoundShot] = Mix_LoadMUS("data/shot.ogg");
	sound[Constants::SoundMine] = Mix_LoadMUS("data/mine.ogg");
	sound[Constants::SoundDropPod] = Mix_LoadMUS("data/droppod.ogg");
	sound[Constants::SoundReload] = Mix_LoadMUS("data/reload.ogg");
	startItem.push_back(UIMenuItem(30, 30, "Name:"));
	startItem.push_back(UIMenuItem(30, 100, "Start Server"));
	startItem.push_back(UIMenuItem(30, 150, "Join"));
	startItem.push_back(UIMenuItem(30, 200, "Server:"));
	startItem.push_back(UIMenuItem(30, 250, "Configure Keys"));
	pauseItem.push_back(UIMenuItem(290, 250, "Resume"));
	pauseItem.push_back(UIMenuItem(290, 300, "Return to Title Screen"));
	pauseItem.push_back(UIMenuItem(290, 350, "Quit"));
}

void Client::run()
{
	menu = std::make_unique<UIMenu>(mainRenderer, ttffont, &startItem);
	runMenu();
	menu = NULL;
	char message[1024];
	int complete = 1;
	SDL_Event sdlevent;
	SDLNet_SocketSet connects = SDLNet_AllocSocketSet(1);
	SDLNet_TCP_AddSocket(connects, server);
	while (complete != 0) {
		int numready = SDLNet_CheckSockets(connects, 5);
		if (numready == 1)
		{
			if (SDLNet_SocketReady(server))
				processMessage();
		}
		if (complete == 1)
		{
			if (state.getPlayer() != NULL)
				complete = 2;
			else
				continue;
		}
		int oldXDir = control.xDir;
		int oldYDir = control.yDir;
		int oldLookXDir = control.lookXDir;
		int oldLookYDir = control.lookYDir;
		int oldAction = action;
		int oldTargetId = targetId;
		while (SDL_PollEvent(&sdlevent))
		{
			int oldCircleButton = control.circleButton;
			int oldL2Button = control.l2Button;
			int oldR2Button = control.r2Button;
			control.process(sdlevent);
			process(sdlevent);
			if (menu)
			{
				if (1 == menu->process(control))
				{
					if (menu->menuState == 0)
						menu = NULL;
					else if (menu->menuState == 1)
					{
						menu = NULL;
						complete = 0;
					}
					else
					{
						SDL_Quit();
						exit(0);
					}
				}
				continue;
			}
			if (control.optionButton == 1)
				menu = std::make_unique<UIMenu>(mainRenderer, ttffont, &pauseItem);
			if (oldCircleButton != control.circleButton && control.circleButton == 1)
				action = Constants::ActionThrowGlowsitck;
			if (oldL2Button != control.l2Button)
			{
				if (control.l2Button == 1)
					action = Constants::ActionMine;
				else if (control.l2Button == 0 && action == Constants::ActionMine)
					action = Constants::ActionNone;
			}
			if (oldR2Button != control.r2Button)
			{
				if (control.r2Button == 1)
					action = Constants::ActionShoot;
				else if (control.r2Button == 0 && action == Constants::ActionShoot)
					action = Constants::ActionNone;
			}
			if (sdlevent.type == SDL_QUIT)
			{
				SDL_Quit();
				exit(0);
			}
		}
		state.updateSight();
		findTarget();
		if (menu)
		{
		}
		else
		{
			if ((oldXDir != control.xDir) || (oldYDir != control.yDir))
			{
				SDLNet_Write16(Message::MovePlayer, message);
				SDLNet_Write16(control.xDir, message + 2);
				SDLNet_Write16(control.yDir, message + 4);
				SDLNet_TCP_Send(server, message, 6);
			}
			if ((oldLookXDir != control.lookXDir) || (oldLookYDir != control.lookYDir))
			{
				SDLNet_Write16(Message::LookPlayer, message);
				SDLNet_Write16(control.lookXDir, message + 2);
				SDLNet_Write16(control.lookYDir, message + 4);
				SDLNet_TCP_Send(server, message, 6);
			}
			if ((oldAction != action) || (oldTargetId != targetId))
			{
				SDLNet_Write16(Message::ActionPlayer, message);
				SDLNet_Write16(action, message + 2);
				SDLNet_Write16(targetId, message + 4);
				SDLNet_TCP_Send(server, message, 6);
				if (action == Constants::ActionThrowGlowsitck)
					action = Constants::ActionNone;
			}
		}
		draw();
		auto e = state.enemies.begin();
		while (e != state.enemies.end())
		{
			if (e->type != Constants::EnemyDropPod && e->health == 0)
			{
				e = state.enemies.erase(e);
			}
			else
				e++;
		}
	}
	if (serverThread)
	{
		SDLNet_Write16(Message::Shutdown, message);
		SDLNet_TCP_Send(server, message, 2);
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		serverThread->join();
		serverThread = NULL;
		server = NULL;
	}
	SDLNet_FreeSocketSet(connects);
	SDLNet_TCP_Close(server);
	state.clear();
}

Mix_Music *Client::getSound(int s)
{
	return sound[s];
}

void Client::draw()
{
	SDL_RenderClear(mainRenderer);
	auto pos = state.getPosition();
	std::pair<int, int> target;
	if (targetId != -1)
	{
		for (auto &e : state.enemies)
		{
			if (e.id == targetId)
			{
				target = std::make_pair(e.x, e.y);
				break;
			}
		}
	}
	SDL_Rect src, dst;
	src.w = 32;
	src.h = 32;
	src.y = 0;
	dst.w = 32;
	dst.h = 32;
	for (int y = 0; y < 17; y++)
	{
		if ((pos.second - 8 + y < 0) || (pos.second - 8 + y >= CaveChunk::h * Constants::ChunkAmountHeight))
			continue;
		for (int x = 0; x < 30; x++)
		{
			if ((pos.first - 15 + x < 0) || (pos.first - 15 + x >= CaveChunk::w * Constants::ChunkAmountWidth))
				continue;
			Visibility::Visible v = state.sight.canSee(pos.first - 15 + x, pos.second - 8 + y);
			if (v != Visibility::Never)
			{
				dst.y = y * 32;
				dst.x = x * 32;
				int tile = state.getTile(pos.first - 15 + x, pos.second - 8 + y);
				src.x = tile * 32;
				SDLScale_RenderCopy(mainRenderer, tileset, &src, &dst);
			}
		}
	}
	for (int y = 0; y < 17; y++)
	{
		if ((pos.second - 8 + y < 0) || (pos.second - 8 + y >= CaveChunk::h * Constants::ChunkAmountHeight))
			continue;
		for (int x = 0; x < 30; x++)
		{
			if ((pos.first - 15 + x < 0) || (pos.first - 15 + x >= CaveChunk::w * Constants::ChunkAmountWidth))
				continue;
			Visibility::Visible v = state.sight.canSee(pos.first - 15 + x, pos.second - 8 + y);
			if (v == Visibility::Present)
			{
				for (auto &e : state.enemies)
				{
					if ((pos.first - 15 + x == e.x) && (pos.second - 8 + y == e.y))
					{
						dst.y = y * 32;
						dst.x = x * 32;
						if (e.type == Constants::EnemyDropPod)
						{
							dst.w = 48;
							dst.h = 48;
							src.w = 48;
							src.h = 48;
							src.x = e.sprite * 48;
							dst.x -= 8;
							dst.y -= 14;
							SDLScale_RenderCopy(mainRenderer, droppodset, &src, &dst);
							dst.w = 32;
							dst.h = 32;
							src.w = 32;
							src.h = 32;
						}
						else
						{
							src.x = e.sprite * 32;
							SDLScale_RenderCopy(mainRenderer, enemyset, &src, &dst);
						}
					}
				}
				for (auto &p : state.players)
				{
					if ((pos.first - 15 + x == p.x) && (pos.second - 8 + y == p.y))
					{
						dst.y = y * 32;
						dst.x = x * 32;
						src.x = p.facing * 32;
						if (p.effect == Constants::PlayerEffectInPod)
						{
							dst.w = 48;
							dst.h = 48;
							src.w = 48;
							src.h = 48;
							src.x = 48;
							dst.x -= 8;
							dst.y -= 14;
							SDLScale_RenderCopy(mainRenderer, droppodset, &src, &dst);
							dst.w = 32;
							dst.h = 32;
							src.w = 32;
							src.h = 32;
						}
						else
						{
							SDLScale_RenderCopy(mainRenderer, playerset, &src, &dst);
						}
					}
				}
				if (pos.first - 15 + x == target.first && pos.second - 8 + y == target.second && target.first > -1)
				{
					src.w = 32;
					src.h = 32;
					src.y = 0;
					src.x = 32;
					dst.w = 32;
					dst.h = 32;
					dst.y = y * 32;
					dst.x = x * 32;
					SDLScale_RenderCopy(mainRenderer, uiset, &src, &dst);
				}
			}
			else if (v == Visibility::Past)
			{
				dst.y = y * 32;
				dst.x = x * 32;
				int tile = Constants::BlockShadow;
				src.x = tile * 32;
				SDLScale_RenderCopy(mainRenderer, tileset, &src, &dst);
			}
		}
	}
	Uint64 ticks = SDL_GetTicks64();
	auto e = effects.begin();
	while (e != effects.end())
	{
		if (e->draw(mainRenderer, this, pos.first, pos.second, ticks))
			e = effects.erase(e);
		else
			e++;
	}
	drawUI();
	if (menu)
		menu->draw(mainRenderer);
	SDL_RenderPresent(mainRenderer);
}

void Client::drawUI()
{
	int totalFuelCrystals = 0;
	SDL_Rect src, dst;
	src.w = nameWidth;
	src.h = nameHeight;
	src.y = 0;
	src.x = 0;
	dst.w = nameWidth;
	dst.h = nameHeight;
	dst.y = 30;
	dst.x = 30;
	SDLScale_RenderCopy(mainRenderer, nametext, &src, &dst);
	PlayerState *p = state.getPlayer();
	if (p)
	{
		SDL_SetRenderDrawColor(mainRenderer, 255, 0, 0, 255);
		dst.x = 30;
		dst.y = 50;
		dst.h = 20;
		dst.w = p->health * 2;
		SDL_RenderFillRect(mainRenderer, &dst);
		SDL_SetRenderDrawColor(mainRenderer, 0, 0, 0, 255);
		if (p->fuelCrystals > 0)
		{
			src.w = fuelcrystalWidth;
			src.h = fuelcrystalHeight;
			src.y = 0;
			src.x = 0;
			dst.w = fuelcrystalWidth;
			dst.h = fuelcrystalHeight;
			dst.x = (960 - fuelcrystalWidth) / 2;
			dst.y = 500;
			SDLScale_RenderCopy(mainRenderer, fuelcrystaltext, &src, &dst);
			src.w = 32;
			src.h = 32;
			src.y = 0;
			src.x = 0;
			dst.w = 32;
			dst.h = 32;
			dst.x = (960 - fuelcrystalWidth) / 2 - 36;
			dst.y = 495;
			SDLScale_RenderCopy(mainRenderer, uiset, &src, &dst);
			numbers->render(mainRenderer, (960 + fuelcrystalWidth) / 2 + 32, 500, p->fuelCrystals);
		}
		numbers->render(mainRenderer, 850, 500, p->magazine);
		dst.w = missionWidth;
		dst.h = missionHeight;
		dst.x = 960 - missionWidth - 50;
		dst.y = 30;
		SDLScale_RenderCopy(mainRenderer, missiontext, NULL, &dst);
		numbers->render(mainRenderer, 910, 30, 50);
		for (auto &p : state.players)
		{
			totalFuelCrystals += p.fuelCrystals;
		}
		if (totalFuelCrystals >= 50)
		{
			totalFuelCrystals = 50;
			dst.w = completeWidth;
			dst.h = completeHeight;
			dst.x = (960 - completeWidth) / 2;
			dst.y = 30;
			SDLScale_RenderCopy(mainRenderer, completetext, NULL, &dst);
			if (p->effect == Constants::PlayerEffectInPod)
			{
				dst.w = wonWidth;
				dst.h = wonHeight;
				dst.x = (960 - wonWidth) / 2;
				dst.y = 200;
				SDLScale_RenderCopy(mainRenderer, wontext, NULL, &dst);
			}
		}
		numbers->render(mainRenderer, 870, 30, totalFuelCrystals);
	}
}

void Client::processMessage()
{
	char message[2048];
	int result;
	int start = 0;
	if (buffer.size() > 0)
		memcpy(message, &buffer[0], buffer.size());
	result = SDLNet_TCP_Recv(server, message + buffer.size(), 2048 - buffer.size());
	if (result < 1)
	{
		printf("SDLNet_TCP_Send: %s\n", SDLNet_GetError());
		exit(2);
	}
	result += buffer.size();
	while (result > 2)
	{
		bool fullMessage = false;
		int cmd = SDLNet_Read16(message + start);
		switch (cmd)
		{
			case Message::NewChunk:
				if (result - 2 >= 4 + CaveChunk::w * CaveChunk::h)
					fullMessage = true;
				break;
			case Message::PlayerStatus:
				if (result >= 2 + PlayerState::messageSize)
					fullMessage = true;
				break;
			case Message::MapChange:
				if (result >= 8)
					fullMessage = true;
				break;
			case Message::EnemyStatus:
				if (result >= 2 + EnemyState::messageSize)
					fullMessage = true;
				break;
			case Message::Effect:
				if (result >= 12)
					fullMessage = true;
				break;
			default:
				break;
		}
		if (fullMessage)
		{
			switch (cmd)
			{
				case Message::NewChunk:
					state.addChunk(message + 2 + start);
					result -= 6 + CaveChunk::w * CaveChunk::h;
					start += 6 + CaveChunk::w * CaveChunk::h;
					break;
				case Message::PlayerStatus:
					state.updatePlayer(message + 2 + start);
					result -= 2 + PlayerState::messageSize;
					start += 2 + PlayerState::messageSize;
					break;
				case Message::MapChange:
					state.mapChange(message + 2 + start);
					result -= 8;
					start += 8;
					break;
				case Message::EnemyStatus:
					state.updateEnemy(message + 2 + start);
					result -= 2 + EnemyState::messageSize;
					start += 2 + EnemyState::messageSize;
					break;
				case Message::Effect:
					effects.push_back(Effect(message + 2 + start));
					result -= 12;
					start += 12;
					break;
				default:
					break;
			}
		}
		else
		{
			memmove(message, message + start, result);
			int result2 = SDLNet_TCP_Recv(server, message + result, 2048 - result);
			if (result2 < 1)
			{
				printf("SDLNet_TCP_Send: %s\n", SDLNet_GetError());
				exit(2);
			}
			result += result2;
			start = 0;
		}
	}
	buffer.resize(result);
	if (result > 0 )
	{
		memcpy(&buffer[0], message + start, buffer.size());
	}
}

std::pair<int, int> Client::findTarget()
{
	auto p = state.getPlayer();
	int dst = 0; // Not real distance just an approximation
	EnemyState *target = NULL;
	switch (p->facing)
	{
		case 0:
			for (auto &e : state.enemies)
			{
				if (e.type < Constants::EnemyFirstHostile)
					continue;
				if (e.y >= p->y)
					continue;
				if (Visibility::Present != state.sight.canSee(e.x, e.y))
					continue;
				if (abs(e.x - p->x) <= (p->y - e.y))
				{
					int curDst = p->y - e.y + abs(e.x - p->x);
					if ((target == NULL) || (dst > curDst))
					{
						target = &e;
						dst = curDst;
					}
				}
			}
			break;
		case 1:
			for (auto &e : state.enemies)
			{
				if (e.type < Constants::EnemyFirstHostile)
					continue;
				if (e.y >= p->y)
					continue;
				if (Visibility::Present != state.sight.canSee(e.x, e.y))
					continue;
				if (e.x > p->x)
				{
					int curDst = p->y - e.y + e.x - p->x;
					if ((target == NULL) || (dst > curDst))
					{
						target = &e;
						dst = curDst;
					}
				}
			}
			break;
		case 2:
			for (auto &e : state.enemies)
			{
				if (e.type < Constants::EnemyFirstHostile)
					continue;
				if (e.x <= p->x)
					continue;
				if (Visibility::Present != state.sight.canSee(e.x, e.y))
					continue;
				if ((e.x - p->x) >= abs(p->y - e.y))
				{
					int curDst = e.x - p->x + abs(e.y - p->y);
					if ((target == NULL) || (dst > curDst))
					{
						target = &e;
						dst = curDst;
					}
				}
			}
			break;
		case 3:
			for (auto &e : state.enemies)
			{
				if (e.type < Constants::EnemyFirstHostile)
					continue;
				if (e.y <= p->y)
					continue;
				if (Visibility::Present != state.sight.canSee(e.x, e.y))
					continue;
				if (e.x > p->x)
				{
					int curDst = p->y - e.y + e.x - p->x;
					if ((target == NULL) || (dst > curDst))
					{
						target = &e;
						dst = curDst;
					}
				}
			}
			break;
		case 4:
			for (auto &e : state.enemies)
			{
				if (e.type < Constants::EnemyFirstHostile)
					continue;
				if (e.y <= p->y)
					continue;
				if (Visibility::Present != state.sight.canSee(e.x, e.y))
					continue;
				if (abs(e.x - p->x) <= (e.y - p->y))
				{
					int curDst = e.y - p->y + abs(e.x - p->x);
					if ((target == NULL) || (dst > curDst))
					{
						target = &e;
						dst = curDst;
					}
				}
			}
			break;
		case 5:
			for (auto &e : state.enemies)
			{
				if (e.type < Constants::EnemyFirstHostile)
					continue;
				if (e.y <= p->y)
					continue;
				if (Visibility::Present != state.sight.canSee(e.x, e.y))
					continue;
				if (e.x < p->x)
				{
					int curDst = e.y - p->y + p->x - e.x;
					if ((target == NULL) || (dst > curDst))
					{
						target = &e;
						dst = curDst;
					}
				}
			}
			break;
		case 6:
			for (auto &e : state.enemies)
			{
				if (e.type < Constants::EnemyFirstHostile)
					continue;
				if (e.x >= p->x)
					continue;
				if (Visibility::Present != state.sight.canSee(e.x, e.y))
					continue;
				if ((p->x - e.x) >= abs(p->y - e.y))
				{
					int curDst = p->x - e.x + abs(e.y - p->y);
					if ((target == NULL) || (dst > curDst))
					{
						target = &e;
						dst = curDst;
					}
				}
			}
			break;
		case 7:
			for (auto &e : state.enemies)
			{
				if (e.type < Constants::EnemyFirstHostile)
					continue;
				if (e.y >= p->y)
					continue;
				if (Visibility::Present != state.sight.canSee(e.x, e.y))
					continue;
				if (e.x < p->x)
				{
					int curDst = p->y - e.y + p->x - e.x;
					if ((target == NULL) || (dst > curDst))
					{
						target = &e;
						dst = curDst;
					}
				}
			}
			break;
		default:
			break;
	}
	if (target)
	{
		targetId = target->id;
		return std::pair(target->x, target->y);
	}
	else
	{
		targetId = -1;
		return std::pair(-1, -1);
	}
}

void Client::runMenu()
{
	char *username = std::getenv("USERNAME");
	if (username != NULL)
		config.name = username;
	startItem[0].setDynamic(mainRenderer, ttffont, config.name);
	startItem[3].setDynamic(mainRenderer, ttffont, config.server);
	bool done = false;
	SDL_Event sdlevent;
	std::string errorMsg;
	control.xButton = 0; // Ugly hack
	while (!done)
	{
		drawMenu(errorMsg);
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
		while (SDL_PollEvent(&sdlevent))
		{
			control.process(sdlevent);
			process(sdlevent);
			if (keySelect)
				keySelect->process(sdlevent);
			if (sdlevent.type == SDL_QUIT)
			{
				SDL_Quit();
				exit(0);
			}
		}
		if (textInput)
		{
			int result = textInput->process(control);
			if (result == 1)
			{
				if (menu->menuState == 0)
				{
					config.name = textInput->text;
					startItem[0].setDynamic(mainRenderer, ttffont, textInput->text);
				}
				else
				{
					config.server = textInput->text;
					startItem[3].setDynamic(mainRenderer, ttffont, textInput->text);
				}
				textInput = NULL;
			}
			else if (result == -1)
				textInput = NULL;
		}
		else if (keySelect)
		{
			int result = keySelect->process(control);
			if (result == 1)
				keySelect = NULL;
			else if (result == -1)
				keySelect = NULL;
		}
		else
		{
			if (1 == menu->process(control))
			{
				if (menu->menuState == 1 || menu->menuState == 2)
					done = true;
				else if (menu->menuState == 0)
					textInput = std::make_unique<UITextInput>(mainRenderer, ttffont, 300, 150, config.name);
				else if (menu->menuState == 3)
					textInput = std::make_unique<UITextInput>(mainRenderer, ttffont, 300, 150, config.server);
				else if (menu->menuState == 4)
					keySelect = std::make_unique<UIKeySelect>(mainRenderer, ttffont, &control);
			}
		}
		if (done)
		{
			if (errorMsg != "")
				errorMsg = "";
			else
			{
				if (menu->menuState == 1)
				{
					serverObj = std::make_unique<Server>();
					serverThread = std::make_unique<std::thread>(&Server::run, *serverObj);
					std::this_thread::sleep_for(std::chrono::milliseconds(1000));
				}
			}
		}
	}
	int len = config.name.length();
	if (len > 20)
	{
		config.name = config.name.substr(0, 20);
		len = config.name.length();
	}
	state.setPlayer(config.name);
	SDL_Surface *img;
	SDL_Color c;
	c.a = 255;
	c.r = 255;
	c.g = 255;
	c.b = 255;
	img = TTF_RenderUTF8_Solid(ttffont, config.name.c_str(), c);
	nameWidth = img->w;
	nameHeight = img->h;
	nametext = SDL_CreateTextureFromSurface(mainRenderer, img);
	SDL_FreeSurface(img);

	IPaddress ip;
	if (SDLNet_ResolveHost(&ip, ((menu->menuState == 1) ? "127.0.0.1" : config.server.c_str()), 9999) == -1) {
		std::printf("SDLNet_ResolveHost: %s\n", SDLNet_GetError());
		std::exit(1);
	}
	server = SDLNet_TCP_Open(&ip);
	if (!server) {
		std::printf("SDLNet_TCP_Open: %s\n", SDLNet_GetError());
		exit(2);
	}
	int result;
	if (len) {

		/* print out the message */
		printf("Sending: %.*s\n", len, config.name.c_str());

		result = SDLNet_TCP_Send(server, config.name.c_str(), len); /* add 1 for the NULL */
		if (result < len)
			printf("SDLNet_TCP_Send: %s\n", SDLNet_GetError());
	}
}

int menu[4] = {30, 100, 150, 200};

void Client::drawMenu(const std::string &errorMsg)
{
	SDL_RenderClear(mainRenderer);
	menu->draw(mainRenderer);
	if (textInput)
		textInput->draw(mainRenderer);
	if (keySelect)
		keySelect->draw(mainRenderer);
	SDL_Texture *errortext = NULL;
	if (errorMsg != "")
	{
		SDL_Color c;
		c.a = 255;
		c.r = 255;
		c.g = 255;
		c.b = 255;
		SDL_Rect dst;
		SDL_Surface *img = TTF_RenderUTF8_Blended_Wrapped(ttffont, errorMsg.c_str(), c, 600);
		SDL_SetRenderDrawColor(mainRenderer, 255, 0, 0, 255);
		dst.x = (960 - img->w - 10) / 2;
		dst.y = 95;
		dst.w = img->w + 20;
		dst.h = img->h + 10;
		SDL_RenderFillRect(mainRenderer, &dst);
		SDL_SetRenderDrawColor(mainRenderer, 0, 0, 0, 255);
		SDL_Texture *errortext = SDL_CreateTextureFromSurface(mainRenderer, img);
		dst.w = img->w;
		dst.h = img->h;
		dst.y = 100;
		dst.x = (960 - img->w) / 2;
		SDLScale_RenderCopy(mainRenderer, errortext, NULL, &dst);
		SDL_FreeSurface(img);
	}
	SDL_RenderPresent(mainRenderer);
	if (errortext)
		SDL_DestroyTexture(errortext);
}

void Client::process(SDL_Event &sdlevent)
{
	if (sdlevent.type == SDL_KEYDOWN)
	{
		if (sdlevent.key.keysym.sym == SDLK_F12)
		{
			fullscreen = !fullscreen;
			if (fullscreen)
			{
				SDL_SetWindowFullscreen(mainWindow, SDL_WINDOW_FULLSCREEN_DESKTOP);
				int xScreen, yScreen;
				SDL_GetWindowSize(mainWindow, &xScreen, &yScreen);
				SDLScale_Set(xScreen / 960, yScreen / 540);
			}
			else
			{
				SDL_SetWindowFullscreen(mainWindow, 0);
				SDLScale_Set(1, 1);
			}
		}
	}
}
