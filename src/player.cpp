#include "player.h"
#include "game.h"
#include "message.h"

static const int moveCooldownDefault = 2;
static int moveModifier[3] = {0, 1, -1};

Player::Player(const std::string &n, std::pair<int, int> start)
: PlayerState(n, start.first, start.second, 4), moveCooldown(0), actionCooldown(0), changed(false), effectDuration(10)
{
	control = std::make_shared<Controller>();
	path.setEndPoint(x, y);
}

bool Player::operator==(const Player &p) const
{
	return name == p.name;
}

bool Player::readyToMove()
{
	if (health == 0)
		return false;
	if (effectDuration > 0)
	{
		if (0 == --effectDuration)
			effect = 0;
		changed = true;
	}
	if (effect == Constants::PlayerEffectInPod)
		return false;
	if (moveCooldown > 0)
		moveCooldown--;
	return moveCooldown == 0;
}

bool Player::readyToAction()
{
	if (health == 0)
		return false;
	if (effect == Constants::PlayerEffectInPod)
		return false;
	if (actionCooldown > 0)
		actionCooldown--;
	return actionCooldown == 0;
}

void Player::move(Game &g)
{
	if (control->xDir == 0 && control->yDir == 0)
		return;
	if (g.isPassable(x + moveModifier[control->xDir], y + moveModifier[control->yDir]))
	{
		x += moveModifier[control->xDir];
		y += moveModifier[control->yDir];
		moveCooldown = moveCooldownDefault;
		path.setEndPoint(x, y);
		changed = true;
	}
	else if (g.isPassable(x + moveModifier[control->xDir], y))
	{
		x += moveModifier[control->xDir];
		moveCooldown = moveCooldownDefault;
		path.setEndPoint(x, y);
		changed = true;
	}
	else if (g.isPassable(x, y + moveModifier[control->yDir]))
	{
		y += moveModifier[control->yDir];
		moveCooldown = moveCooldownDefault;
		path.setEndPoint(x, y);
		changed = true;
	}
}

void Player::act(Game &g)
{
	if (control->action == Constants::ActionMine)
	{
		if (g.mine(x + Constants::facingMap[facing][0], y + Constants::facingMap[facing][1], this))
		{
			actionCooldown = Constants::MineCooldown;
			if (moveCooldown < actionCooldown)
				moveCooldown = actionCooldown;
		}
	}
	else if (control->action == Constants::ActionShoot)
	{
		if (magazine > 0)
		{
			bool found = false;
			for (auto &e : g.enemies)
			{
				if (e.id == control->targetId)
				{
					e.addHealth(-Constants::gunDamage);
					g.listener->effect(Constants::EffectShot, x, y, e.x, e.y);
					found = true;
				}
			}
			if (!found)
			{
				int endX = x;
				int endY = y;
				for (int i = 0; i < 6; i++)
				{
					if (g.isPassable(endX + Constants::facingMap[facing][0], endY + Constants::facingMap[facing][1]))
					{
						endX += Constants::facingMap[facing][0];
						endY += Constants::facingMap[facing][1];
					}
					else
					{
						break;
					}
				}
				g.listener->effect(Constants::EffectShot, x, y, endX, endY);
			}
			addMagazine(-1);
			actionCooldown = Constants::GunCooldown;
		}
	}
	else if (control->action == Constants::ActionThrowGlowsitck)
	{
		Enemy e(g.nextId(), Constants::EnemyGlowStick, x, y);
		e.target = facing;
		g.enemies.push_back(e);
		g.listener->enemyStatus(e);
		control->action = Constants::ActionNone;
	}
	else if (control->action == Constants::ActionNone && magazine == 0)
	{
		actionCooldown = Constants::GunCooldown;
		addMagazine(Constants::MagazineSize - magazine);
		g.listener->effect(Constants::EffectReload, x, y, 0, 0);
	}
}

void Player::calcFacing(int xDir, int yDir)
{
	int oldFacing = facing;
	if (health == 0)
		facing = 8;
	else if ((xDir == 0) && (yDir == 2))
		facing = 0;
	else if ((xDir == 1) && (yDir == 2))
		facing = 1;
	else if ((xDir == 1) && (yDir == 0))
		facing = 2;
	else if ((xDir == 1) && (yDir == 1))
		facing = 3;
	else if ((xDir == 0) && (yDir == 1))
		facing = 4;
	else if ((xDir == 2) && (yDir == 1))
		facing = 5;
	else if ((xDir == 2) && (yDir == 0))
		facing = 6;
	else if ((xDir == 2) && (yDir == 2))
		facing = 7;
	if (facing != oldFacing)
		changed = true;
}

int Player::addFuelCrystals(int c)
{
	changed = true;
	fuelCrystals += c;
	return 0;
}

int Player::addHealth(int h)
{
	changed = true;
	health += h;
	if (health < 0)
	{
		health = 0;
		facing = 8;
	}
	return 0;
}

int Player::addMagazine(int b)
{
	changed = true;
	magazine += b;
	if (magazine < 0)
		magazine = 0;
	return 0;
}
