#ifndef __UIKEYSELECT_H
#define __UIKEYSELECT_H
#include <SDL.h>
#include <SDL_ttf.h>
#include <string>

class ClientController;

class UIKeySelect
{
public:
	UIKeySelect(SDL_Renderer *r, TTF_Font *font, ClientController *cc);
	~UIKeySelect();

	void process(SDL_Event &sdlevent);
	int process(ClientController &control);
	void draw(SDL_Renderer *r);

private:
	TTF_Font *f;
	ClientController *control;
	SDL_Keycode keys[15];
	SDL_Texture *promptTex[15];
	int promptSize[15][2];
	SDL_Texture *keyTex[15];
	int keySize[15][2];
	int current;
};

#endif
