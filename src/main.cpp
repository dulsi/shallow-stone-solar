#include "server.h"
#include "client.h"

#include <cstdio>
#include <cstring>

int main(int argc, char *argv[])
{
	if ((argc > 1) && (0 == std::strcmp(argv[1], "server")))
	{
		Server s;
		s.run();
	}
	else
	{
		Client c;
		while (true)
		{
			c.run();
		}
	}
	return 0;
}
