#ifndef __SERVER_H
#define __SERVER_H

#include <SDL.h>
#include <SDL_net.h>
#include <memory>
#include <vector>
#include "game.h"
#include "gamelistener.h"
#include "controller.h"
#include "player.h"

class Server : public GameListener
{
public:
	Server();
	void run();
	void addPossible();
	bool processClient(std::pair<TCPsocket, std::shared_ptr<Controller>> &c);
	void processPossible(TCPsocket &c);

	void addChunk(int x, int y, CaveChunk &chunk);
	void playerStatus(Player &p);
	void enemyStatus(Enemy &e);
	void mapChange(int x, int y, int wall);
	void effect(int type, int x1, int y1, int x2, int y2);

private:
	void sendCurrentState(TCPsocket &c);
	void send(TCPsocket &c, char *message, int len);
	void tick();

private:
	static TCPsocket server;
	TCPsocket timer;
	SDLNet_SocketSet connects;
	std::vector<TCPsocket> possibles;
	std::vector<std::pair<TCPsocket, std::shared_ptr<Controller>>> clients;
	Game g;
};

#endif
