#include "drawnumbers.h"
#include "sdlscale.h"

DrawNumbers::DrawNumbers(SDL_Renderer *r, TTF_Font *ttffont)
{
	int i;
	SDL_Surface *num[10];
	SDL_Color c;
	c.a = 255;
	c.r = 255;
	c.g = 255;
	c.b = 255;
	char n[2];
	n[1] = 0;
	int w = 0;
	int h = 0;
	for (i = 0; i < 10; i++)
	{
		n[0] = '0' + i;
		num[i] = TTF_RenderUTF8_Solid(ttffont, n, c);
		numberSize.push_back(std::pair(num[i]->w, num[i]->h));
		where.push_back(w);
		w += num[i]->w;
		if (h < num[i]->h)
			h = num[i]->h;
	}
	Uint32 rmask, gmask, bmask, amask;
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	rmask = 0xff000000;
	gmask = 0x00ff0000;
	bmask = 0x0000ff00;
	amask = 0x000000ff;
#else
	rmask = 0x000000ff;
	gmask = 0x0000ff00;
	bmask = 0x00ff0000;
	amask = 0xff000000;
#endif

	SDL_Rect dst;
	dst.y = 0;
	SDL_Surface *img = SDL_CreateRGBSurface(0, w, h, 32, rmask, gmask, bmask, amask);
	for (i = 0; i < 10; i++)
	{
		dst.x = where[i];
		dst.w = numberSize[i].first;
		dst.h = numberSize[i].second;
		SDL_BlitSurface(num[i], NULL, img, &dst);
		SDL_FreeSurface(num[i]);
	}
	numbers = SDL_CreateTextureFromSurface(r, img);
	SDL_FreeSurface(img);
}

void DrawNumbers::render(SDL_Renderer *r, int x, int y, int num)
{
	int numDigit = 1;
	int tmp;
	for (tmp = num / 10; tmp > 0; tmp = tmp / 10)
	{
		numDigit++;
	}
	int digit;
	SDL_Rect src, dst;
	for (tmp = numDigit; tmp > 0; tmp--)
	{
		digit = num;
		for (int i = 1; i < tmp; i++)
			digit = digit / 10;
		digit = digit % 10;
		src.w = numberSize[digit].first;
		src.h = numberSize[digit].second;
		src.y = 0;
		src.x = where[digit];
		dst.w = numberSize[digit].first;
		dst.h = numberSize[digit].second;
		dst.y = y;
		dst.x = x;
		SDLScale_RenderCopy(r, numbers, &src, &dst);
		x += numberSize[digit].first;
	}
}
