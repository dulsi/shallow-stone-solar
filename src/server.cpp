#include "server.h"
#include "message.h"
#include <cstdio>
#include <cstring>
#include <thread>
#include <chrono>

TCPsocket Server::server = NULL;

Server::Server()
{
	if (SDL_Init(0) == -1) {
		std::printf("SDL_Init: %s\n", SDL_GetError());
		std::exit(1);
	}
	if (SDLNet_Init() == -1) {
		std::printf("SDLNet_Init: %s\n", SDLNet_GetError());
		std::exit(2);
	}
	IPaddress ip;
	if (SDLNet_ResolveHost(&ip, NULL, 9999) == -1) {
		std::printf("SDLNet_ResolveHost: %s\n", SDLNet_GetError());
		std::exit(1);
	}
	if (server == NULL)
	{
		server = SDLNet_TCP_Open(&ip);
		if (!server) {
			std::printf("SDLNet_TCP_Open: %s\n", SDLNet_GetError());
			std::exit(2);
		}
	}
	connects = SDLNet_AllocSocketSet(16);
	SDLNet_TCP_AddSocket(connects, server);
	timer = NULL;
}

void Server::run()
{
	bool done = false;
	std::thread t5(&Server::tick, *this);
	g.addListener(this);
	while (!done) {
		int numready = SDLNet_CheckSockets(connects, -1);
		if (numready < 1)
		{
			done = true;
			break;
		}
		for (auto &c : clients)
		{
			if (SDLNet_SocketReady(c.first))
			{
				done = done || processClient(c);
			}
		}
		if ((timer) && (SDLNet_SocketReady(timer)))
		{
			char message[10];
			int len = SDLNet_TCP_Recv(timer, message, 10);
			g.runTurn();
		}
		for (auto &p : possibles)
		{
			if (SDLNet_SocketReady(p))
			{
				processPossible(p);
			}
		}
		if (SDLNet_SocketReady(server))
			addPossible();
	}
	SDLNet_TCP_Send(timer, "X", 1);
	t5.join();
	SDLNet_FreeSocketSet(connects);
	SDLNet_TCP_Close(timer);
//	SDLNet_TCP_Close(server);
	for (auto &each : possibles)
	{
		SDLNet_TCP_Close(each);
	}
	for (auto &each : clients)
	{
		SDLNet_TCP_Close(each.first);
	}
}

void Server::addPossible()
{
	TCPsocket client = SDLNet_TCP_Accept(server);
	if (!client) 
		return;
	possibles.push_back(client);
	SDLNet_TCP_AddSocket(connects, client);

	/* get the clients IP and port number */
	IPaddress *remoteip;
	remoteip = SDLNet_TCP_GetPeerAddress(client);
	if (!remoteip) {
		std::printf("SDLNet_TCP_GetPeerAddress: %s\n", SDLNet_GetError());
	}
	else
	{
		/* print out the clients IP and port number */
		Uint32 ipaddr;
		ipaddr = SDL_SwapBE32(remoteip->host);
		std::printf("Accepted a connection from %d.%d.%d.%d port %hu\n", ipaddr >> 24,
			(ipaddr >> 16) & 0xff, (ipaddr >> 8) & 0xff, ipaddr & 0xff,
			remoteip->port);
	}
}

bool Server::processClient(std::pair<TCPsocket, std::shared_ptr<Controller>> &c)
{
	char message[1024];
	int len = SDLNet_TCP_Recv(c.first, message, 1024);
	if (!len) {
		std::printf("SDLNet_TCP_Recv: %s\n", SDLNet_GetError());
		SDLNet_TCP_DelSocket(connects, c.first);
		clients.erase(std::remove(clients.begin(), clients.end(), c));
		return false;
	}
	return c.second->processMessages(len, message);
}

void Server::processPossible(TCPsocket &c)
{
	char message[1024];
	int len = SDLNet_TCP_Recv(c, message, 1024);
	if (!len) {
		std::printf("SDLNet_TCP_Recv: %s\n", SDLNet_GetError());
		SDLNet_TCP_DelSocket(connects, c);
		possibles.erase(std::remove(possibles.begin(), possibles.end(), c));
		return;
	}
	if ((len == 5) && (0 == std::strncmp(message, "TIMER", 5)))
	{
		timer = c;
		possibles.erase(std::remove(possibles.begin(), possibles.end(), c));
	}
	else
	{
		clients.push_back(std::pair(c, g.getController(std::string(message, len))));
		possibles.erase(std::remove(possibles.begin(), possibles.end(), c));
		std::printf("First Received: %.*s\n", len, message);
		sendCurrentState(c);
	}
}

void Server::addChunk(int x, int y, CaveChunk &chunk)
{
	char message[2048];
	SDLNet_Write16(Message::NewChunk, message);
	SDLNet_Write16(x, message + 2);
	SDLNet_Write16(y, message + 4);
	std::memcpy(message + 6, &chunk.caveMap[0], CaveChunk::w * CaveChunk::h);
	for (auto &c : clients)
	{
		SDLNet_TCP_Send(c.first, message, 6 + CaveChunk::w * CaveChunk::h);
	}
}

void Server::playerStatus(Player &p)
{
	char message[128];
	SDLNet_Write16(Message::PlayerStatus, message);
	p.writeMessage(message + 2);
	for (auto &c : clients)
	{
		SDLNet_TCP_Send(c.first, message, 2 + PlayerState::messageSize);
	}
}

void Server::enemyStatus(Enemy &e)
{
	char message[128];
	SDLNet_Write16(Message::EnemyStatus, message);
	e.writeMessage(message + 2);
	for (auto &c : clients)
	{
		SDLNet_TCP_Send(c.first, message, 2 + EnemyState::messageSize);
	}
}

void Server::mapChange(int x, int y, int wall)
{
	char message[128];
	SDLNet_Write16(Message::MapChange, message);
	SDLNet_Write16(x, message + 2);
	SDLNet_Write16(y, message + 4);
	SDLNet_Write16(wall, message + 6);
	for (auto &c : clients)
	{
		SDLNet_TCP_Send(c.first, message, 8);
	}
}

void Server::effect(int type, int x1, int y1, int x2, int y2)
{
	char message[128];
	SDLNet_Write16(Message::Effect, message);
	SDLNet_Write16(type, message + 2);
	SDLNet_Write16(x1, message + 4);
	SDLNet_Write16(y1, message + 6);
	SDLNet_Write16(x2, message + 8);
	SDLNet_Write16(y2, message + 10);
	for (auto &c : clients)
	{
		SDLNet_TCP_Send(c.first, message, 12);
	}
}

void Server::sendCurrentState(TCPsocket &c)
{
	char message[2048];
	for (auto &chunk : g.world)
	{
		SDLNet_Write16(Message::NewChunk, message);
		int loc = chunk.first;
		SDLNet_Write16(loc % Constants::ChunkAmountWidth, message + 2);
		SDLNet_Write16(loc / Constants::ChunkAmountWidth, message + 4);
		std::memcpy(message + 6, &chunk.second.caveMap[0], CaveChunk::w * CaveChunk::h);
		send(c, message, 6 + CaveChunk::w * CaveChunk::h);
	}
	for (auto &p : g.players)
	{
		SDLNet_Write16(Message::PlayerStatus, message);
		p.writeMessage(message + 2);
		send(c, message, 2 + PlayerState::messageSize);
		if (p.effect == Constants::PlayerEffectInPod)
		{
			SDLNet_Write16(Message::Effect, message);
			SDLNet_Write16(Constants::EffectDropPod, message + 2);
			SDLNet_Write16(p.x, message + 4);
			SDLNet_Write16(p.y, message + 6);
			SDLNet_Write16(0, message + 8);
			SDLNet_Write16(0, message + 10);
			send(c, message, 12);
		}
	}
	for (auto &e : g.enemies)
	{
		SDLNet_Write16(Message::EnemyStatus, message);
		e.writeMessage(message + 2);
		send(c, message, 2 + EnemyState::messageSize);
	}
}

void Server::send(TCPsocket &c, char *message, int len)
{
	int offset = 0;
	while (len > 0)
	{
		int sent = SDLNet_TCP_Send(c, message + offset, len);
		if (sent < 0)
		{
			exit(2);
		}
		len -= sent;
		offset += sent;
	}
}

void Server::tick()
{
	IPaddress ip;
	if (SDLNet_ResolveHost(&ip, "localhost", 9999) == -1) {
		std::printf("SDLNet_ResolveHost: %s\n", SDLNet_GetError());
		std::exit(1);
	}
	TCPsocket server = SDLNet_TCP_Open(&ip);
	if (!server) {
		std::printf("SDLNet_TCP_Open: %s\n", SDLNet_GetError());
		exit(2);
	}
	int result = SDLNet_TCP_Send(server, "TIMER", 5);
	SDLNet_SocketSet socketset = SDLNet_AllocSocketSet(16);
	SDLNet_TCP_AddSocket(socketset, server);
	bool done = false;
	while (!done)
	{
		int numready = SDLNet_CheckSockets(socketset, 67);
		if (numready != 0)
			done = true;
		result = SDLNet_TCP_Send(server, "1", 1);
	}
	SDLNet_TCP_Close(server);
}
