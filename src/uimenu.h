#ifndef __UIMENU_H
#define __UIMENU_H
#include <SDL.h>
#include <SDL_ttf.h>
#include <string>
#include <vector>

class ClientController;

class UIMenuItem
{
public:
	UIMenuItem(int xPos, int yPos, const std::string &text);
	void createTexture(SDL_Renderer *r, TTF_Font *font);
	void destroyTexture();
	void setDynamic(SDL_Renderer *r, TTF_Font *font, const std::string &text);

	int x;
	int y;
	std::string textStatic;
	std::string textDynamic;
	SDL_Texture *textStaticTex;
	int textStaticW;
	int textStaticH;
	SDL_Texture *textDynamicTex;
	int textDynamicW;
	int textDynamicH;
};

class UIMenu
{
public:
	UIMenu(SDL_Renderer *r, TTF_Font *font, std::vector<UIMenuItem> *i);
	~UIMenu();

	int process(ClientController &control);
	void draw(SDL_Renderer *r);

	int menuState;

private:
	TTF_Font *f;
	std::vector<UIMenuItem> *items;
	int moveCooldown;
};

#endif
