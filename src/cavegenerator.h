#ifndef __CAVEGENERATOR_H
#define __CAVEGENERATOR_H
/*
 * Automata procedual dungeon generation proof-of-concept
 *
 *
 * Developed by Adam White 
 * https://csharpcodewhisperer.blogspot.com
 * https://github.com/AdamWhiteHat/Roguelike-Procedual-Cave-Generator
 * Converted to C++ by Dennis Payne
 * License: Apache-2.0 License
 * 
 * 
 */

#include <cstddef>
#include <vector>

class CaveChunk
{
public:
	CaveChunk();

	int getAdjacentWalls(int x,int y,int scopeX,int scopeY);
	bool isWall(int x,int y);
	bool isOutOfBounds(int x, int y);
	void followWall(int &x, int &y, int &dir);

	std::vector<std::byte> caveMap;
	static int w;
	static int h;
};

class CaveGenerator
{
public:
	CaveGenerator();

	void addMinerals(CaveChunk &caveMap);
	void smooth(CaveChunk &caveMap);
	void randomFill(CaveChunk &caveMap);
	void print(CaveChunk &caveMap);
	void addUnbreakable(CaveChunk &caveMap, bool north, bool east, bool south, bool west);

private:
	std::byte placeWallLogic(CaveChunk &caveMap, int x,int y);

private:
	int percentAreWalls;
};

#endif
