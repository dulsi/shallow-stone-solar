#include <SDL.h>
#include <SDL_net.h>
#include <cstring>
#include "playerstate.h"
#include "message.h"

int PlayerState::messageSize(35);

PlayerState::PlayerState(const std::string &n, int xPos, int yPos, int f)
: name(n), x(xPos), y(yPos), facing(f), health(100), fuelCrystals(0), effect(Constants::PlayerEffectInPod), magazine(Constants::MagazineSize)
{
}

PlayerState::PlayerState(const std::string &n, char *message)
: name(n)
{
	readMessage(message);
}

void PlayerState::readMessage(char *message)
{
	// name already read and set
	x = SDLNet_Read16(message + 21);
	y = SDLNet_Read16(message + 23);
	facing = SDLNet_Read16(message + 25);
	health = SDLNet_Read16(message + 27);
	fuelCrystals = SDLNet_Read16(message + 29);
	effect = SDLNet_Read16(message + 31);
	magazine = SDLNet_Read16(message + 33);
}

void PlayerState::writeMessage(char *message)
{
	std::memset(message, 0, 21);
	std::memcpy(message, name.c_str(), name.length());
	SDLNet_Write16(x, message + 21);
	SDLNet_Write16(y, message + 23);
	SDLNet_Write16(facing, message + 25);
	SDLNet_Write16(health, message + 27);
	SDLNet_Write16(fuelCrystals, message + 29);
	SDLNet_Write16(effect, message + 31);
	SDLNet_Write16(magazine, message + 33);
}
